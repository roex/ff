var express = require('express');
var router = express.Router();
var http = require('http');
var https = require('https');
var xml2js = require('xml2js');
var apicache = require('apicache');
//var goods = [];

//
// !!! Страница возвращает список товаров из фида !!!
//

//router.get('/', apicache.middleware('5 minutes'), function(req, res, next) {
router.get('/', function(req, res, next) {

    //TODO Использовать sku_base_id
    var ids = [15520, 15521];

    var options = {
        host: 'www.clinique.ru',
        path: '/lcl/google_shopping_feed_ru.xml'
    };
    var request = https.request(options, function (reqRes) {
        var data = '';
        reqRes.on('data', function (chunk) {
            data += chunk;
        });
        reqRes.on('end', function () {
            //console.log(data);

            var goods = [];
            var parser = new xml2js.Parser();
            parser.parseString(data.substring(0, data.length), function (err, result) {

                if (result && typeof result['rss'] !== "undefined") {
                    var items = result['rss']['channel'][0]['item'];
                    for(var i = 0; i < items.length; i++) {
                        goods.push(items[i]);
                        // var prodBaseId = parseInt(items[i]['g:prodbaseID'][0]);
                        // if (ids.indexOf(prodBaseId) > -1) {
                        //     goods.push(items[i]);
                        // }
                    }

                    res.send({ products: goods });
                }
                else {
                    console.log('Ошибка! Попробуйте позже...');
                }
            });


            // parser.parseString(data, function (err, result) {
            //     products = result;
            //     console.dir(result);
            //     console.log('Done');
            // });

        });
    });
    request.on('error', function (e) {
        console.log(e.message);
    });
    request.end();




});

module.exports = router;
