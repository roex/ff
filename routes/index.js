var express = require('express');
var router = express.Router();
var url = require('url');
// var _ = require('underscore');

/* GET home page. */
router.get('/', function (req, res, next) {



    // Filling with defaults values
    var params = {
        'og-title': 'Играй и получай подарки от Concept Сlub!',
        'og-name': '',
        'og-desc': 'Переворачивай волшебные карты, находи пары одинаковых и получи скидку на новогодний шоппинг в интернет-магазине, а также предсказание на Новый год!',
        'og-url': 'http://magic.conceptclub.ru/',
        'og-image': 'http://magic.conceptclub.ru/images/vk/' + req.params.id + '.jpg'
    };

    var fullUrl = req.protocol + '://' + req.get('host');
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;

    //console.log(fullUrl);
    //console.log(query);

     var index = 0,
         image = '';


     var isShare   = typeof query['share'] !== "undefined" && parseInt(query['share']) === 1,
         isProduct = typeof query['product'] !== "undefined" && parseInt(query['product']) > 0,
         isImage   = typeof query['image'] !== "undefined" && query['image'] !== '',
         isQ1      = typeof query['q1'] !== "undefined" && parseInt(query['q1']) >= 1 && parseInt(query['q1']) <= 15,
         isQ2      = typeof query['q2'] !== "undefined" && (query['q2'] === '' || (parseInt(query['q2']) >= 1 && parseInt(query['q2']) <= 2)),
         isQ3      = typeof query['q3'] !== "undefined" && parseInt(query['q3']) >= 1 && parseInt(query['q3']) <= 3,
         isQ4      = typeof query['q4'] !== "undefined" && parseInt(query['q4']) >= 1 && parseInt(query['q4']) <= 4,
         isQ5      = typeof query['q5'] !== "undefined" && parseInt(query['q5']) >= 1 && parseInt(query['q5']) <= 4,
         isQ6      = typeof query['q6'] !== "undefined" && (query['q6'] === '' || (parseInt(query['q6']) >= 1 && parseInt(query['q6']) <= 7));

     if (isShare &&
         isProduct &&
         isImage &&
         isQ1 &&
         isQ2 &&
         isQ3 &&
         isQ4 &&
         isQ5 &&
         isQ6) {
         index = 1;
         image = query['image'];
     }

    /*var isShare   = typeof query['share'] !== "undefined" && parseInt(query['share']) === 1,
        isImage   = typeof query['image'] !== "undefined" && query['image'] !== '';

    if (isShare && isImage) {
        index = 1;
        image = query['image'];
    }*/

    var params = [
        {
            'title': 'Моё тональное средство Clinique',
            'ogtitle': 'Моё тональное средство Clinique',
            'ogdesc': 'Подобрать тональное средство – легко! Тест из 6 шагов от Clinique.',
            'ogurl': fullUrl,
            'ogimage': '',
            //'result': result
        },
        {
            'title': 'Моё тональное средство Clinique',
            'ogtitle': 'Моё тональное средство Clinique',
            'ogdesc': 'Подобрать тональное средство – легко! Тест из 6 шагов от Clinique.',
            'ogurl': fullUrl + req.url,
            'ogimage': fullUrl + '/build/img/products/' + image,
            //'result': result
        }
    ];

    // Render view
    res.render('index', params[index]);
});

module.exports = router;
