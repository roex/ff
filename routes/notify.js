var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/db.db');
var request = require('request');

var timestamp = function () {
    return Math.floor(Date.now() / 1000);
};


// tables: codes10, codes15, codes20
// (code, used, email, sent, subscribe, time)
// CREATE TABLE codes10 (code TEXT, used INTEGER, email TEXT, sent INTEGER, subscribe INTEGER, time TEXT);

// sqlite> .mode csv
// sqlite> .import test.csv foo

// INCOMING: http://magic.conceptclub.ru/sendcode?email=arseny@deasign.ru&subscribe=true&matches=4
// OUTCOMING: localhost:8888?needsub=0&email=arseny@deasign.ru&promo=AAA-BBB-CCC

router.get('/', function(req, res, next) {

    var saved = false;
    var isAjax = req.xhr;

    // Check for AJAX and params
    if (!isAjax || !req.query.email || !req.query.sku || !req.query.prodbaseid) {
        console.log('ERROR: some parameters are missing / not an AJAX call');
        res.send({ sent: false });
        return;
    }

    console.log('STATUS: staring to query database');
    db.serialize(function() {


        db.run("INSERT INTO notifications (email, prod_base_id, sku_base_id, export, created_at) VALUES (?,?,?,?,?)",
            [
                req.query.email,
                req.query.sku,
                req.query.prodbaseid,
                0,
                timestamp(),
            ]);

        saved = true;


    });


    res.send({ saved: saved });
});



module.exports = router;
