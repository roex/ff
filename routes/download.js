var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/db.db');
var request = require('request');
var fs = require('fs');

var timestamp = function () {
  return new Date().getTime();
};

// type = (notify|results)
// http://127.0.0.1:1337/download?type=notifications&from=2017-10-01&to=2017-10-05
router.get('/', function (req, res, next) {

  //req.query.from

  var select = '',
      from = null,
      to = null,
      type = 'notifications';

  if (req.query.from) {
    //var queryFrom = req.query.from.split('-');
    from = new Date(req.query.from + ' 00:00:00').getTime() / 1000;

    //from = req.query.from;
  }

  if (req.query.to) {
    //var queryTo = req.query.to.split('-');
    to = new Date(req.query.to + ' 23:59:00').getTime() / 1000;

    //to = req.query.to;
  }

  if (req.query.type)
      type = req.query.type;

  if (type === 'results' || type === 'notifications') {

      if (from && to) {
          select = 'select * from ' + type + ' where created_at != "" and created_at >=' + from + ' and created_at <=' + to;
      }
      else if (from && !to) {
          select = 'select * from ' + type + ' where created_at != "" and created_at >=' + from;
      }
      else if (!from && to) {
          select = 'select * from ' + type + ' where created_at != "" and created_at <=' + to;
      }
      else {
          select = 'select * from ' + type + ' where created_at != ""';
      }

      db.serialize(function () {

          db.all(select, function (err, rows) {
              if (!err) {

                  var fileName = timestamp();
                  // Found code
                  var file = fs.createWriteStream('public/storage/csv/' + fileName + '.csv');
                  file.on('error', function(err) { /* error handling */ });

                  rows.forEach(function (row) {
                      if (type === 'notifications')
                          file.write(row.email + ';' + row.prod_base_id + ';SKU ' + row.sku_base_id + ' ' + '\n');
                      else if (type === 'results')
                          file.write(row.email + '\n');
                  });

                  file.end();

                  //res.redirect('/get/' + fileName + '?file=' + fileName);
                  res.redirect('/get/' + fileName);
              }
              else {
                  console.log(err);
                  res.send({'msg': 'Error!'});
              }
          });

      });

  }
  else {
      res.send({'msg': 'Error!'});
  }


});


module.exports = router;