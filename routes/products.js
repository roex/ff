var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/db.db');
var request = require('request');

//
// !!! Страница возвращает список всех товаров с вариантами ответов для фильтрации !!!
//

// tables: results
// (id, email, answers, subscribed, updated_at, created_at)

// INCOMING: http://example.com/load?email=test@mail.ru

router.get('/', function(req, res, next) {

    var isAjax = req.xhr;

    //Check for AJAX and params
    if (!isAjax) {
        console.log('ERROR: some parameters are missing / not an AJAX call');
        res.send({ results: false });
        return;
    }

    // Load results by email
    console.log('STATUS: staring to query database');
    db.serialize(function() {
        db.all('SELECT * FROM products', function (err, rows) {
            if (!err) {
                /*for (var i=0; i<rows.length; i++) {
                    for (var property in rows[i]) {
                        if (rows[i].hasOwnProperty(property) && rows[i][property].indexOf('[') > -1) {
                            rows[i][property] = JSON.parse(rows[i][property]);
                            // do stuff
                        }
                    }
                }*/
                res.send({ results: rows });
            }
            else {
                console.log(err);
                res.send({ results: null });
            }
        });
    });


});



module.exports = router;
