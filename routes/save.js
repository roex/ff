var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/db.db');
var request = require('request');

var timestamp = function () {
    return Math.floor(Date.now() / 1000);
};


// tables: codes10, codes15, codes20
// (code, used, email, sent, subscribe, time)
// CREATE TABLE codes10 (code TEXT, used INTEGER, email TEXT, sent INTEGER, subscribe INTEGER, time TEXT);

// sqlite> .mode csv
// sqlite> .import test.csv foo

// INCOMING: http://magic.conceptclub.ru/sendcode?email=arseny@deasign.ru&subscribe=true&matches=4
// OUTCOMING: localhost:8888?needsub=0&email=arseny@deasign.ru&promo=AAA-BBB-CCC

router.get('/', function(req, res, next) {

    var saved = false;
    var isAjax = req.xhr;

    // Check for AJAX and params
    if (!isAjax || !req.query.id  || !req.query.email || !req.query.answers) {
        console.log('ERROR: some parameters are missing / not an AJAX call');
        res.send({ sent: false });
        return;
    }

    console.log('STATUS: staring to query database');
    db.serialize(function() {

        if (typeof req.query.id !== "undefined" && parseInt(req.query.id) > 0) {
            db.get('select * from results where id=' + req.query.id + ' limit 1', function (err, row) {
                if (!err) {
                    // Found
                    console.log('STATUS: found id: ' + row.id);

                    db.run('UPDATE results SET prod_base_id = $pbid, image = $image, total = $total, ' +
                        'answers = $answers, subscriber = $subscriber, link = $link, ' +
                        'updated_at = $updatedAt WHERE id = $id AND email = $email', {
                        $pbid: req.query.pbid,
                        $image: req.query.image,
                        $answers: req.query.answers,
                        $subscriber: typeof req.query.subscriber === "undefined" ? 0 : req.query.subscriber,
                        $total: req.query.total,
                        $link: req.query.link,
                        $updatedAt: timestamp(),
                        $id: req.query.id,
                        $email: req.query.email
                    });

                    saved = true;
                }
                else {
                    saved = false;
                }
            });
        }
        else {

            db.run("INSERT INTO results (prod_base_id, image, email, answers, subscriber, total, link, updated_at, created_at) VALUES (?,?,?,?,?,?,?,?,?)",
                [
                    req.query.pbid,
                    req.query.image,
                    req.query.email,
                    req.query.answers,
                    typeof req.query.subscriber === "undefined" ? 0 : req.query.subscriber,
                    req.query.total,
                    req.query.link,
                    timestamp(),
                    timestamp(),
                ]);

            saved = true;
        }


    });


    res.send({ saved: saved });
});



module.exports = router;
