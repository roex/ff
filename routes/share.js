var express = require('express');
var router = express.Router();

router.get('/:image?', function(req, res, next) {

   /*var result = typeof req.params.id !== "undefined" ? req.params.id : 1,
       share = {
            1: {
                'title': 'Прогрессивный JavaScript-фреймворк!',
                'ogtitle': 'Vue.js',
                'ogdesc': 'Прогрессивный JavaScript-фреймворк!',
                'ogurl': 'https://ru.wikipedia.org/',
                'ogimage': 'https://ru.vuejs.org/images/logo.png',
                'result': result
            },
            2: {
                'title': 'Свободная энциклопедия',
                'ogtitle': 'Wikipedia',
                'ogdesc': 'Свободная энциклопедия',
                'ogurl': 'https://ru.wikipedia.org/',
                'ogimage': 'https://ru.wikipedia.org/static/images/project-logos/ruwiki.png',
                'result': result
            }
        },
        params = typeof share[result] !== "undefined" ? share[result] : share[1];*/

   var params = {
       'title': 'Моё тональное средство Clinique',
       'ogtitle': 'Моё тональное средство Clinique',
       'ogdesc': 'Подобрать тональное средство – легко! Тест из 6 шагов от Clinique.',
       'ogurl': 'http://cl-finder.herokuapp.com',
       'ogimage': 'http://www.clinique.ru/media/export/cms/products/402x464/' + req.params.image,
       //'result': result
   };


    res.render('results.html', params);
});

module.exports = router;
