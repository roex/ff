var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./routes/db.db');
var request = require('request');

//
// !!! Страница возвращает результаты пользователя !!!
//

// table: results
// (id, email, answers, subscribed, updated_at, created_at)

// INCOMING: http://example.com/load?email=test@mail.ru

router.get('/', function(req, res, next) {

    var isAjax = req.xhr;

    //Check for AJAX and params
    if (!isAjax) {
        console.log('ERROR: some parameters are missing / not an AJAX call');
        res.send({ results: false });
        return;
    }

    // Load results by email
    if (typeof req.query.email !== "undefined" && req.query.email !== '') {
        console.log('STATUS: staring to query database');
        db.serialize(function() {
            db.all('SELECT * FROM results WHERE email="' + req.query.email + '" ORDER BY date(created_at) LIMIT 10', function (err, rows) {
                if (!err) {
                    res.send({ results: rows });
                }
                else {
                    console.log(err);
                    res.send({ results: null });
                }
            });
        });
    }
    else {
        res.send({ results: null });
    }


});



module.exports = router;
