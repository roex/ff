// register modal component
/*Vue.component('modal', {
 template: '#modal-template'
 })*/

// start app
/*
 new Vue({
 el: '#app',
 data: {
 showModal: false
 }
 })*/
var owlFunc = function () {
    if ($('.js-owl').length > 0) {
        var owl = $('.js-owl');
        if ($(window).width() < 641) {
            if ($('.js-owl .owl-item').length > 0) {
                /*$('.cl-card__owl-item').each(function (index) {
                    owl.trigger('remove.owl.carousel', index);
                });*/
                $('.js-owl').html('');
                owl.trigger('refresh.owl.carousel');
                owl.trigger('destroy.owl.carousel');
            }
        }
        else {

            if ($('.js-owl .owl-item').length > 0)
                return;

            owl.owlCarousel({
                margin: 0,
                items: 1,
                loop: false,
                nav: true,
                dots: false,
                autoHeight: true,
                //nestedItemSelector: '.owl-item',
                //stageElement: 'div.js-owl',
                navText: [
                    '<i class="fa fa-angle-left"></i>',
                    '<i class="fa fa-angle-right"></i>'
                ],
                autoWidth: false
            });

            $('.cl-card__owl-item').each(function (index) {
                owl.trigger('add.owl.carousel', ['<div class="owl-item">' + $(this).html() + '</div>']).trigger('refresh.owl.carousel');
            });
        }
    }
};
$(document).ready(function () {

    //owlFunc();

    function autoHeightAnimate(element, time) {
        var curHeight = element.height(), // Get Default Height
            autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({height: autoHeight}, parseInt(time)); // Animate to Auto Height
    }

    $('a.js-read-more').on('click touch', function (e) {
        e.preventDefault();
        var $block = $('.cl-card__body .cl-card__description'),
            animateTime = 500;

        if ($block.height() === 92) {
            $(this).html('Скрыть описание <i class="ico"></i>');
            autoHeightAnimate($block, animateTime);

        } else {
            $(this).html('Читать все описание <i class="ico"></i>');
            $block.stop().animate({height: '92px'}, animateTime);

        }
        return false;
    });

    $('.js-collapse-hide').on('click', function () {
        $('.collapse').collapse('hide');
    });
});

$(window).resize(function () {
    owlFunc();
});