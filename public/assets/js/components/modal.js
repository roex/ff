Vue.component('modal', {
    template: '#modal-template',
    props: ['variations', 'variation'],
    data: function() {
        return {
            internalVariation: null
        }
    },
    watch: {
        'internalVariation': function() {
            // When the internal value changes, we $emit an event. Because this event is
            // named 'input', v-model will automatically update the parent value
            this.$emit('input', this.internalValue);
        }
    },
    methods: {
        changeCurrentVariation: function (skuBaseID) {
            var availableResults = this.$parent.availableResults;
            for (var i = 0; i < availableResults.length; i++) {
                var availableResult = availableResults[i];
                if (parseInt(availableResult['sku_base_id']) === parseInt(skuBaseID)) {
                    this.$parent.currentVariation = availableResult;
                    break;
                }
            }
        },
    }
});