
// register modal component
Vue.component('load', {
    template: '#page-load',
    data: function () {
        return {
            results: this.$parent.userResults
        }
    },
    methods: {
        load: function (index) {
            var arr = this.$parent.userResults.filter(function(val) { return val });
            for (var i=0; i<arr.length; i ++) {
                if (parseInt(arr[i]['id']) === parseInt(index)) {
                    this.$parent.userResult = JSON.parse(arr[i].answers);
                    break;
                }
            }
            this.$emit('loaded');
            //this.$emit('close');
        }
    }
});