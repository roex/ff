
var checkboxes = {
    template: '<div> \
    <h2 class="cl-test__title" v-html="question"></h2> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-wrapper\'"> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-col\'" v-for="(option, index) in options"> \
        <div v-bind:class="\'cl-test__\'+prefix+\'-item\'"> \
            <input v-bind:name="id"  \
                   v-bind:value="option.value"  \
                   v-bind:id="\'id-\' + id + \'-\' + option.id" \
                   :disabled="isDisabled(option.value)" \
                   class="js-answer" \
                   v-on:click="updateValue($event.target.name, $event.target.value, $event.target.checked)"  \
                   type="checkbox"> \
          <label v-bind:for="\'id-\' + id + \'-\' + option.id" v-html="option.text"></label>  \
        </div>  \
    </div>  \
    </div>  \
    <a href="javascript:;" v-show="isAnswered" v-on:click="finish" class="cl-test__link-skip">К результатам <i class="ico"></i></a>\
    <a href="javascript:;" v-show="isNotAnswered" v-on:click="skip" class="cl-test__link-skip">Вы можете пропустить этот шаг <i class="ico"></i></a>\
    </div>',
    props: ['id', 'question', 'options', 'prefix'],
    computed: {
        isNotAnswered: function () {
            var tempArr = this.$parent.userResponses[this.id];
            if (tempArr === null) {
                return true;
            }
            else {
                if (tempArr.filter(function(val) { return val }).length === 0) {
                    return true;
                }
            }
            return false;
        },
        isAnswered: function() {
            return this.$parent.userResponses[this.id] !== null && this.$parent.userResponses[this.id].filter(function(val) { return val }).length  > 0;
        },
    },
    methods: {
        isDisabled: function(value) {

            //console.log(value);
            //console.log(this);

            var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                //totalQuestions = this.$parent.quiz.questions.length,
                inResults = false;

            // if (totalAnswered === totalQuestions)
            //     return true;

            if (totalAnswered >= 1) {
                var questionIndex = this.id + 1,
                    allVariationsInQuestion = [];

                var availableVariations = this.$parent.availableVariations;
                for (var i = 0; i < availableVariations.length; i++) {
                    var result = availableVariations[i];
                    if (result['question_' + questionIndex] instanceof Array) {
                        allVariationsInQuestion = allVariationsInQuestion.concat(result['question_' + questionIndex]);
                    }
                    else {
                        allVariationsInQuestion.push(parseInt(result['question_' + questionIndex]));
                    }
                }

                inResults = allVariationsInQuestion.indexOf(parseInt(value)) > -1;

            }

            // Если тест пройден
            if (this.$parent.isEnd) {
                // Считаем сколько элементов в массиве и бликруем если больше нужного
                if (this.$parent.userResponses[this.id] !== null) {
                    var tempArr = this.$parent.userResponses[this.id].filter(function(val) { return val });
                    return tempArr.length >= 2 && tempArr.indexOf(value+'') === -1;
                }
                else {
                    return false;
                }
            }
            else {
                if (inResults) {
                    // Считаем сколько элементов в массиве и бликруем если больше нужного
                    if (this.$parent.userResponses[this.id] !== null) {
                        var tempArr = this.$parent.userResponses[this.id].filter(function(val) { return val });
                        return tempArr.length >= 2 && tempArr.indexOf(value+'') === -1;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }


        },
        updateValue: function (name, value, checked) {
            var userResponses = this.$parent.userResponses,
                res = userResponses[name] === null ? [] : userResponses[name].filter(function(val) { return val });

            if (checked) {
                res.push(value);
            } else {
                var index = res.indexOf(value);
                if (index > -1)
                    res.splice(index, 1);
            }

            this.$emit('input', res);
        },
        skip: function () {
            this.$parent.isEnd = true;
            this.$emit('skip');
            this.$emit('finish');
        },
        finish: function () {
            this.$parent.isEnd = true;
            this.$emit('finish');
        }
    }
};
