
var radios = {
    template: '<div>\
    <h2 class="cl-test__title" v-html="question"></h2> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-wrapper\'"> \
        <div v-bind:class="\'cl-test__\'+prefix+\'-col\'" v-for="(option, index) in options">  \
            <div v-bind:class="\'cl-test__\'+prefix+\'-item\'">  \
                <input v-bind:name="id"  \
                       v-bind:value="option.value"  \
                       class="js-answer"\
                       v-bind:id="\'id-\' + id + \'-\' + option.id"\
                        :disabled="isDisabled(option.value)" \
                       v-on:click="updateValue($event.target.name, $event.target.value)"  \
                       type="radio"> \
                <label v-bind:for="\'id-\' + id + \'-\' + option.id" v-html="option.text"></label>  \
            </div>  \
        </div>  \
        </div>  \
    </div>',
    props: ['id', 'question', 'options', 'prefix'],
    components: {},
    methods: {
        isDisabled: function(value) {
            var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                totalQuestions = this.$parent.quiz.questions.length;

            if (totalAnswered === totalQuestions)
                return false;

            if (totalAnswered >= 1) {
                var questionIndex = this.id + 1,
                    allVariationsInQuestion = [];

                var availableVariations = this.$parent.availableVariations;
                for (var i=0; i<availableVariations.length; i++) {
                    var result = availableVariations[i];
                    if (result['question_' + questionIndex] instanceof Array) {
                        allVariationsInQuestion = allVariationsInQuestion.concat(result['question_' + questionIndex]);
                    }
                    else {
                        allVariationsInQuestion.push(parseInt(result['question_' + questionIndex]));
                    }
                }


                if (allVariationsInQuestion.indexOf(parseInt(value)) > -1)
                    return false;
                else
                    return true;

            }
        },
        updateValue: function (name, value) {
            // var res = this.$parent.userResponses[name].filter(function(val) { return val }),
            //     isEdit = false;

            res = [];
            res.push(value);
            this.$emit('input', res);

            if (this.$parent.userResponses[name] !== null) {
                var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                    totalQuestions = this.$parent.quiz.questions.length;
                if (totalAnswered === totalQuestions) {
                    this.$emit('edit');
                }
                else {
                    this.$emit('change');
                }
            }
            else {
                this.$emit('answer');
            }

        },
    }
};
