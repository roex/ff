

Vue.component('save', {
    template: '#page-save',
    data: function () {
        return {
            agreeWithProcessing: true,
            wantReceiveInformation: true,
            saveEmail: ''
        }
    },
    /*computed: {
        isEmailEmpty: function () {
            return this.agreeWithProcessing;
        }
    },*/
    methods: {

        goToResults: function () {
            this.$parent.showPage('results');
        },

        save: function () {

            var self = this;

            var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
            if (this.saveEmail == '' || !re.test(this.saveEmail)) {
                alert('Введите email!');
            }
            else {

                var params = {
                    id: this.$parent.userResult,
                    pbid: parseInt(this.$parent.results.bigProduct.id),
                    total: 1 + this.$parent.results.smallProducts.length,
                    link: encodeURI(this.$parent.results.bigProduct.link),
                    image: encodeURI(this.$parent.results.bigProduct.image),
                    email: this.saveEmail,
                    answers: JSON.stringify(this.$parent.userResponses),
                    subscriber: this.wantReceiveInformation
                };

                $.ajax({
                    url: '/save',
                    data: params,
                    success: function (data) {
                        // test@mail.ru
                        //self.$emit('close');
                        self.$emit('success');
                    }
                });

            }

        },
    }
});
