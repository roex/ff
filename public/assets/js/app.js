if (!Array.prototype.fill) {
    Object.defineProperty(Array.prototype, 'fill', {
        value: function(value) {

            // Steps 1-2.
            if (this == null) {
                throw new TypeError('this is null or not defined');
            }

            var O = Object(this);

            // Steps 3-5.
            var len = O.length >>> 0;

            // Steps 6-7.
            var start = arguments[1];
            var relativeStart = start >> 0;

            // Step 8.
            var k = relativeStart < 0 ?
                Math.max(len + relativeStart, 0) :
                Math.min(relativeStart, len);

            // Steps 9-10.
            var end = arguments[2];
            var relativeEnd = end === undefined ?
                len : end >> 0;

            // Step 11.
            var final = relativeEnd < 0 ?
                Math.max(len + relativeEnd, 0) :
                Math.min(relativeEnd, len);

            // Step 12.
            while (k < final) {
                O[k] = value;
                k++;
            }

            // Step 13.
            return O;
        }
    });
}


// components/test.js

// components/shareButtons.js
//= components/radios.js
//= components/checkboxes.js
//= components/quiz.js
//= components/load.js
//= components/save.js
//= components/success.js
// components/modal.js


new Vue({
    el: '#app',
    data: {
        // Store current question index
        questionIndex: -1,
        isEnd: false,

        // Main data
        quiz: quiz,

        // Prepare results
        results: {
            'bigProduct': {
                'id': 0,
                'title': '',
                'image': '',
                'link': '',
                'description': '',
                'weight': 0,
                'price': 0,
                'lh1': '',
                'lh2': '',
                'review1': '',
                'review2': '',
                'variations': [],
                'variationsAll': [],
                'variationsMain': [],
            }, // with variations
            'smallProducts': [], // without variations
            'lifehack': 0,
            'empty': {
                'ids': [
                    9285,
                    6201,
                    42938,
                    5276,
                    39658,
                    45511
                ],
                'filtered': []
            },
        },

        // An array initialized with "null" values for each question
        // It means: "did the user answered correctly to the question n?" "no".
        userResponses: Array(quiz.questions.length).fill(null),

        // Used for loading results
        userEmail: '',
        // Used for notifications
        notificationEmail: '',

        // Store current page
        currentPage: 'quiz',

        // Used for page with results (buy button, variation title, etc.)
        currentVariation: null,

        // Show/hide some elements
        showModal: false, // show modal window with variations
        showNotificationModal: false, // show modal window with notification (when product is not available)
        showNotificationSuccessModal: false, // show success modal window
        showEditMenu: false, // show menu on page with results

        //showResults: false,
        //showSaveForm: false,
        //showSuccess: false,

        // Последние 3 результата пользователя
        userResults: {},
        // Если пользователь выбрал конкретный резултат, то сохраняем его ID здесь
        userResult: 0,


        //saveEmail: '',
        //agreeWithProcessing: false,
        //wantReceiveInformation: false,

        // Этот параметр используется для шаринга
        //result: 2,

        // Products from feed
        products: {},
        //productsTree: {},

        productsFromDB: {},

        // Available results from db
        availableResults: {},

        // Filtered available results from db
        availableVariations: [],
        availableVariationsTree: {},
        //availableProducts: [],
        //bigProduct: null,
    },
    created: function () {

        var self = this;

        //TODO We should use when

        // load available results
        $.get('/variations', function(data) {
            if (data && data['results']) {
                var res = data['results'];
                // convert string to array
                for (var i = 0; i < res.length; i++) {
                    for (var property in res[i]) {
                        if (res[i].hasOwnProperty(property) && (res[i][property]+'').indexOf('[') > -1) {
                            res[i][property] = JSON.parse(res[i][property]);
                        }
                    }
                }
                self.availableResults = data['results'];
                for (var i = 0; i < self.availableResults.length; i++) {
                    var variation = self.availableResults[i];
                    if (typeof self.availableVariationsTree[variation['prod_base_id']] === "undefined")
                        self.availableVariationsTree[variation['prod_base_id']] = [];

                    self.availableVariationsTree[variation['prod_base_id']].push(variation);
                }
            }
        }, 'json');

        // load available products from db
        $.get('/products', function(data) {
            if (data && data['results']) {
                for (var i = 0; i < data['results'].length; i++) {
                    var id = data['results'][i]['prod_base_id'];
                    self.productsFromDB[id] = data['results'][i];
                    self.productsFromDB[id]['id'] = id;
                    self.productsFromDB[id]['price'] = self.price(data['results'][i]['price']);
                }
            }
        }, 'json');

        // load products from feed
        $.get('/feed', function(data) {
            if (data && data['products']) {
                self.products = data['products'];

                /*for (var i = 0; i < self.products.length; i++) {
                    var product = self.products[i],
                        id = product['g:prodbaseID'][0];

                    if (typeof self.productsTree[id] === "undefined") {
                        //console.log(product);
                        self.productsTree[id] = {
                            'id': product['g:prodbaseID'][0],
                            'title': product['title'][0],
                            'description': product['description'][0],
                            'image': product['g:image_link'][0],
                            'link': product['link'][0],
                            'size': typeof product['g:size'] !== "undefined" ? product['g:size'][0] : '',
                            'price': self.price(product['g:price'][0]),
                            'variations': [],
                            'variationsAll': [],
                            'variationsMain': [],
                            'variationsMore': [],
                        };
                    }

                    self.productsTree[id]['variations'].push(product);
                }*/

                $('body').addClass('loaded');

                var isShare = self.getParameterByName('share');
                if (parseInt(isShare) === 1) {
                    self.userResult = -1;
                    self.userResponses = [
                        [self.getParameterByName('q1')],
                        [self.getParameterByName('q2')],
                        [self.getParameterByName('q3')],
                        [self.getParameterByName('q4')],
                        [self.getParameterByName('q5')],
                        self.getParameterByName('q6') && self.getParameterByName('q6') !== '' ? self.getParameterByName('q6').split(' ') : [],
                    ];
                    self.goToResults();
                    self.setValues(self.userResponses);
                }
            }
        }, 'json');

    },
    // The view will trigger these methods on click
    components: {
        'checkboxes': checkboxes,
        'radios': radios,
        //'test': test
    },
    methods: {
        // Go to next question
        next: function() {
            this.questionIndex++;
        },
        begin: function () {
            this.showPage('quiz');
            this.next();
        },
        // Go to the first question (play again)
        again: function () {
            this.showPage('quiz');
            // Uncheck all radios and checkboxes
            var inputs = document.getElementsByClassName('js-answer');
            for(var i = 0; i<inputs.length; i++){
                inputs[i].checked = false;
            }
            this.userResponses = Array(quiz.questions.length).fill(null);
            this.questionIndex = 0;
            this.userResult = 0;
            this.isEnd = false;
            this.clearPrevResults();
            //this.results.bigProduct['id'] = 0;
            this.showPage('quiz');
            //$('.collapse').collapse('hide');
        },
        // Go to question
        go: function (questionNumber) {
            var self = this;
            this.questionIndex = questionNumber;
            this.findResults();
            this.showPage('quiz');
        },
        // Skip the question
        skip: function() {
            this.userResponses[this.questionIndex] = [];
            this.next();
        },
        // Go to previous question
        prev: function() {
            this.questionIndex--;
        },

        // Helpers
        // declOfNum(count, ['найдена', 'найдено', 'найдены']);
        declOfNum: function (number, titles) {
            cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        },
        number_format: function (number, decimals, dec_point, thousands_sep) {
            // http://kevin.vanzonneveld.net
            // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +     bugfix by: Michael White (http://getsprink.com)
            // +     bugfix by: Benjamin Lupton
            // +     bugfix by: Allan Jensen (http://www.winternet.no)
            // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // +     bugfix by: Howard Yeend
            // +    revised by: Luke Smith (http://lucassmith.name)
            // +     bugfix by: Diogo Resende
            // +     bugfix by: Rival
            // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
            // +   improved by: davook
            // +   improved by: Brett Zamir (http://brett-zamir.me)
            // +      input by: Jay Klehr
            // +   improved by: Brett Zamir (http://brett-zamir.me)
            // +      input by: Amir Habibi (http://www.residence-mixte.com/)
            // +     bugfix by: Brett Zamir (http://brett-zamir.me)
            // +   improved by: Theriault
            // +   improved by: Drew Noakes
            // *     example 1: number_format(1234.56);
            // *     returns 1: '1,235'
            // *     example 2: number_format(1234.56, 2, ',', ' ');
            // *     returns 2: '1 234,56'
            // *     example 3: number_format(1234.5678, 2, '.', '');
            // *     returns 3: '1234.57'
            // *     example 4: number_format(67, 2, ',', '.');
            // *     returns 4: '67,00'
            // *     example 5: number_format(1000);
            // *     returns 5: '1,000'
            // *     example 6: number_format(67.311, 2);
            // *     returns 6: '67.31'
            // *     example 7: number_format(1000.55, 1);
            // *     returns 7: '1,000.6'
            // *     example 8: number_format(67000, 5, ',', '.');
            // *     returns 8: '67.000,00000'
            // *     example 9: number_format(0.9, 0);
            // *     returns 9: '1'
            // *    example 10: number_format('1.20', 2);
            // *    returns 10: '1.20'
            // *    example 11: number_format('1.20', 4);
            // *    returns 11: '1.2000'
            // *    example 12: number_format('1.2000', 3);
            // *    returns 12: '1.200'
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                toFixedFix = function (n, prec) {
                    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                    var k = Math.pow(10, prec);
                    return Math.round(n * k) / k;
                },
                s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        },
        price: function (string) {
            /*var arr = string.split(' '),
                price = arr[1];*/
            return this.number_format(string, 0, '', ' ') + ' руб.';
        },
        showPage: function (page) {
            this.scrollTop();
            this.currentPage = page;
        },
        objToArray: function (object) {
            var array = [];
            for (var property in object) {
                if (object.hasOwnProperty(property)) {
                    array.push(object[property]);
                }
            }
            return array;
        },
        shuffleArray: function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        },
        convertTimestamp: function(timestamp) {
            var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
                yyyy = d.getFullYear(),
                mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
                dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
                hh = d.getHours(),
                h = hh,
                min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
                ampm = 'AM',
                time;

            if (hh > 12) {
                h = hh - 12;
                ampm = 'PM';
            } else if (hh === 12) {
                h = 12;
                ampm = 'PM';
            } else if (hh == 0) {
                h = 12;
            }

            // ie: 2013-02-18, 8:35 AM
            //time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
            time = dd+ '/' + mm + '/' + yyyy;

            return time;
        },
        scrollTop: function () {
            window.scrollTo(0, 0);
        },
        goToLastQuestion: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            var lastQuestionIndex = this.userResponses.filter(function(val) { return val }).length;
            this.go(lastQuestionIndex);
        },
        goToNextQuestion: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            this.next();
        },
        goToResults: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            this.finish();
        },
        changeCurrentVariation: function (skuBaseID) {
            var availableResults = this.availableResults;
            for (var i = 0; i < availableResults.length; i++) {
                var availableResult = availableResults[i];
                if (parseInt(availableResult['sku_base_id']) === parseInt(skuBaseID)) {
                    this.currentVariation = availableResult;
                    break;
                }
            }
        },
        showError: function (el, msg) {
            var inputEl = document.getElementById(el);
            var errorEl = document.createElement('div');
            errorEl.innerHTML = msg;
            //errorEl.style.top = inputEl.offsetTop - 17 + 'px';
            errorEl.style.top = (parseInt(inputEl.offsetTop) + parseInt(inputEl.offsetHeight)) + 'px';
            errorEl.style.left = inputEl.offsetLeft + 'px';
            errorEl.className += " h-error";

            //console.log(inputEl.offsetLeft);
            //inputEl.parentNode.insertBefore(errorEl, inputEl.firstChild);
            inputEl.parentNode.appendChild(errorEl);

            setTimeout(function () {
                errorEl.parentNode.removeChild(errorEl);
            }, 3000);

            // append theKid to the end of theParent
            //theParent.appendChild(theKid);

            // prepend theKid to the beginning of theParent
        },
        prepareShareUrl: function () {

            // http://example.com/?share=1&product=PRODBASEID&image=FILENAME.png&q1=1&q2=1&q3=2&q4=1&q5=1+7

            var queryString = window.location.href + '?share=1';

            queryString += '&product=' + this.results.bigProduct['id'];

            var arr = this.results.bigProduct.image.split('/'),
                image = arr[arr.length - 1];

            queryString += '&image=' + image;

            for (var i = 1; i <= this.userResponses.length; i++) {
                queryString += '&q' + i + '=' + this.userResponses[i-1].join('+');
            }

            //var queryString = 'http://www.clinique.ru/tonalnie-sredstva#4';

            return queryString;
        },
        share: function (type) {
            var resUrl = this.prepareShareUrl();
            //console.log(resUrl);

            switch (type) {
                case 'vk':
                    url = 'http://vkontakte.ru/share.php?';
                    url += 'url=' + encodeURIComponent(resUrl);
                    url += '&noparse=false';
                    break;
                case 'fb':
                    url = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(resUrl);
                    break;
            }


            if (url != '')
                window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        },
        debug: function() {
            //console.log(this.questionIndex);
            //this.findResults();
            // console.log(this.availableResults);
            //console.log(this.userResponses);
            //return 0;
            //return this.userResponses.filter(function(val) { return val }).length;
        },

        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////

        findResults: function () {

            if (!this.availableResults.length)
                return;

            var self = this;

            self.availableVariations = self.availableResults.filter(function (result) {
                var userResponses = self.userResponses.filter(function(val) { return val });
                for (var i = 0; i < userResponses.length; i++) { // пропускаем последний вопрос
                    var answers = userResponses[i],
                        questionIndex = i + 1;

                    if (answers === null || answers.length === 0 || questionIndex === 6)
                        continue;

                    for (var j=0; j<answers.length; j++) {

                        var answer = parseInt(answers[j]);

                        if (result['question_' + questionIndex] instanceof Array) {
                            var tempArray = result['question_' + questionIndex].filter(function(val) { return val });
                            if  (tempArray.indexOf(parseInt(answer)) === -1)
                                return false;
                        }
                        else {
                            if (parseInt(result['question_' + questionIndex]) !== parseInt(answer))
                                return false;
                        }

                    }
                }

                //console.log('answers', self.availableVariations);


                // Фильтруем чекбоксы
                if (typeof userResponses[5] !== "undefined" && userResponses[5] !== null && userResponses[5].length > 0) {
                    var answersForLastQuestion = userResponses[5],
                        availableResultsForLastQuestion = result['question_6'].filter(function(val) {
                            return answersForLastQuestion.indexOf(val + "") !== -1;
                        });
                    return availableResultsForLastQuestion.length > 0
                }

                return true;
            });

        },
        loadMoreProducts: function () {
            var self = this,
                moreProducts = [],
                products = self.shuffleArray(self.results['empty']['ids']);

            self.results['empty']['filtered'] = [];

            if (products.length > 3)
                products.splice(3, products.length - 1);

            for (var i = 0; i < products.length; i++) {
                var prodBaseId = products[i];
                moreProducts.push(self.productsFromDB[prodBaseId]);
            }
            return moreProducts;
        },
        clearPrevResults: function () {
            this.results = {
                'bigProduct': {
                    'id': 0,
                    'title': '',
                    'image': '',
                    'description': '',
                    'weight': 0,
                    'price': 0,
                    'lh1': '',
                    'lh2': '',
                    'review1': '',
                    'review2': '',
                    'variations': [],
                    'variationsAll': [],
                    'variationsMain': [],
                }, // with variations
                'smallProducts': [], // without variations
                'lifehack': 0,
                'empty': {
                    'ids': [
                        9285,
                        6201,
                        42938,
                        5276,
                        39658,
                        45511
                    ],
                    'filtered': []
                },
            };
            this.results['empty']['filtered'] = this.loadMoreProducts();
        },
        prepareResults: function () {
            var self = this,
                prodbaseIDs = [];
            //console.log('prepareResults');

            // prepare products for empty result
            this.results['empty']['filtered'] = self.loadMoreProducts();

            // find variations with sku
            // var filteredVariations = self.products.filter(function (product) {
            //     var variations = self.availableVariations;
            //     for (var i = 0; i < variations.length; i++) {
            //         var variation = variations[i];
            //         if (product['g:mpn'][0] === 'SKU' + variation['sku_base_id']) {
            //             return true;
            //         }
            //     }
            //     return false;
            // });

            // return nothing if we found nothing
            // if (!filteredVariations.length) {
            //     self.clearPrevResults();
            //     return;
            // }

            if (!self.availableVariations.length) {
                self.clearPrevResults();
                return;
            }

            // get prodbaseIDs from available variations
            prodbaseIDs = self.availableVariations.map(function (product) {
                //return parseInt(product['g:prodbaseID'][0]);
                return parseInt(product['prod_base_id']);
            });
            // remove duplicates
            prodbaseIDs = prodbaseIDs.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
            // shuffle IDs
            prodbaseIDs = self.shuffleArray(prodbaseIDs);

            // products with lifehack should be first
            var productWithLifeHackIDs = [];
            // find variations with lifehack
            var variationsWithLifeHack = self.availableVariations.filter(function (variation) {
                return parseInt(variation['lh']) > 0;
            });
            if (variationsWithLifeHack.length > 0) {
                // get prodbaseIDs
                productWithLifeHackIDs = variationsWithLifeHack.map(function (product) {
                    return product['prod_base_id'];
                });
            }

            var bigProductID = 0,
                lifeHackIndex = 0,
                loadedProdbaseID = 0;

            if (parseInt(self.userResult) > 0) {
                var userResult = this.userResults.filter(function(val) { return parseInt(val['id']) === parseInt(self.userResult) });
                loadedProdbaseID = userResult[0]['prod_base_id'];
            }

            if (loadedProdbaseID > 0 && prodbaseIDs.indexOf(loadedProdbaseID) > -1 && self.results['bigProduct']['id'] === 0) {
                bigProductID = loadedProdbaseID;
            }
            else {
                // if we have products with lifehack
                if (productWithLifeHackIDs.length > 0) {
                    // get one of them
                    bigProductID = productWithLifeHackIDs[Math.floor(Math.random()*productWithLifeHackIDs.length)];
                }
                else {
                    // get one ID from prodbaseIDs
                    bigProductID = prodbaseIDs[Math.floor(Math.random()*prodbaseIDs.length)];
                }
            }


            if (parseInt(self.userResult) === -1) {
                bigProductID = parseInt(self.getParameterByName('product'));
            }

            // check if everything is ok
            if (bigProductID === 0) {
                self.clearPrevResults();
                return;
            }

            // set lifehack number
            /*if (variationsWithLifeHack.length > 0) {
                for (var i = 0; i < variationsWithLifeHack.length; i++) {
                    var variationWithLifeHack = variationsWithLifeHack[i];
                    if (parseInt(variationWithLifeHack['prod_base_id']) === parseInt(bigProductID)) {
                        lifeHackIndex = variationWithLifeHack['lh'];
                        break;
                    }
                }
            }*/

            // remove bigProductID from prodbaseIDs
            var bigProductIDIndex = prodbaseIDs.indexOf(bigProductID);
            if (bigProductIDIndex > -1)
                prodbaseIDs.splice(bigProductIDIndex, 1);

            // remove IDs if more then 3
            if (prodbaseIDs.length > 3)
                prodbaseIDs = prodbaseIDs.splice(3, prodbaseIDs.length - 3);

            self.results['bigProduct'] = self.productsFromDB[bigProductID];

            // we need variationsTree to attach properties to bigProduct variations
            var variationsTree = {};
            for (var i = 0; i < self.availableVariations.length; i++) {
                var variation = self.availableVariations[i],
                    prodBaseId = variation['prod_base_id'],
                    SKUBaseId = 'SKU' + variation['sku_base_id'];
                if (typeof variationsTree[prodBaseId] === "undefined")
                    variationsTree[prodBaseId] = {};
                if (typeof variationsTree[prodBaseId][SKUBaseId] === "undefined")
                    variationsTree[prodBaseId][SKUBaseId] = {};
                variationsTree[prodBaseId][SKUBaseId] = variation;
            }

            // get bigProduct variations from feed
            var variationSKUs = [];
            var variations = self.products.filter(function (variation) {
                return parseInt(variation['g:prodbaseID'][0]) === parseInt(self.results['bigProduct']['id']);
            });
            if (variations.length > 0) {
                variationSKUs = variations.map(function (product) {
                    return parseInt(product['g:mpn'][0].replace('SKU', ''));
                });
            }

            // get bigProduct variations from results (main variations)
            var variationsMain = self.availableVariations.filter(function (variation) {
                return parseInt(variation['prod_base_id']) === parseInt(self.results['bigProduct']['id']);
            });
            // set variationsMain
            var variationsMainSKUs = [],
                bigProductVariationsMain = [],
                currentVariation = {};
            if (variationsMain.length > 0 && variationSKUs.length > 0) {
                for (var i = 0; i < variationsMain.length; i++) {
                    var variation = variationsMain[i],
                        variationId = variation['sku_base_id'];
                    // check if user can buy variation
                    variation['canBuy'] = variationSKUs.indexOf(variationId) > -1;

                    // show some variations
                    bigProductVariationsMain.push(variation);
                    // and remember them
                    variationsMainSKUs.push(variationId);

                    if (i === 0)
                        currentVariation = variation;
                }
            }
            // set default variation
            self.currentVariation = currentVariation;

            self.results['bigProduct']['variationsMain'] = bigProductVariationsMain;

            // get bigProduct variations from db (all variations)
            var variationsAll = self.availableResults.filter(function (variation) {
                return parseInt(variation['prod_base_id']) === parseInt(self.results['bigProduct']['id']);
            });
            var bigProductVariationsAll = [],
                bigProductVariationsMore = [];
            if (variationsAll.length > 0 && variationSKUs.length > 0) {
                for (var i = 0; i < variationsAll.length; i++) {
                    var variation = variationsAll[i],
                        variationId = variation['sku_base_id'];

                    // check if user can buy variation
                    variation['canBuy'] = variationSKUs.indexOf(variationId) > -1;

                    //
                    bigProductVariationsAll.push(variation);

                    // hide other variations
                    if (variationsMainSKUs.indexOf(variationId) === -1) {
                        bigProductVariationsMore.push(variation);
                    }

                }
            }

            self.results['bigProduct']['variationsAll'] = bigProductVariationsAll;
            self.results['bigProduct']['variationsMore'] = bigProductVariationsMore;

            // set small products
            var smallProducts = [];
            for (var i = 0; i < prodbaseIDs.length; i++) {
                var prodbaseID = prodbaseIDs[i];
                if (typeof self.productsFromDB[prodbaseID] !== "undefined")
                    smallProducts.push(self.productsFromDB[prodbaseID]);
            }
            self.results['smallProducts'] = smallProducts;

            //
            //
            //~~~
            //console.log('results', self.results);

        },
        finish: function () {
            this.prepareResults();
            this.showPage('results');
            this.questionIndex = this.quiz.questions.filter(function(val) { return val }).length;
            //console.log('end!');
        },
        // check radios and checkboxes
        setValues: function (answers) {
            var inputs = document.getElementsByClassName('js-answer');
            for(var i = 0; i<inputs.length; i++){
                var fieldName = inputs[i].name;
                if (typeof answers[fieldName] !== "undefined") {
                    var fieldValue = inputs[i].value;
                    if (answers[fieldName].indexOf(fieldValue) > -1) {
                        inputs[i].checked = true;
                    }
                }
            }
        },
        // Show user's result
        openResults: function () {
            var self = this;

            if (!self.userEmail) {
                self.showError('email-bottom', 'Пожалуйста, введите свой email.');
                self.showError('email-top', 'Пожалуйста, введите свой email.');
                return;
            }

            $.ajax({
                url: '/load',
                data: {email: self.userEmail},
                success: function (data) {
                    if (data && data.results) {
                        var results = data.results;

                        if (results.length === 0) {
                            self.showError('email-bottom', 'Попробуйте другой email или пройдите тест заново.');
                            self.showError('email-top', 'Попробуйте другой email или пройдите тест заново.');
                            self.userEmail = '';
                            return;
                        }

                        if (results.length > 3)
                            results.splice(0,  data.results.length - 3);

                        for (var i = 0; i < results.length; i++) {
                            results[i]['created_at'] = self.convertTimestamp(results[i]['created_at']);
                            results[i]['total'] = 'Найдено ' + results[i]['total'] + ' ' + self.declOfNum(parseInt(results[i]['total']), ['средство', 'средства', 'средств']);
                        }
                        self.userResults = results;
                        self.userEmail = '';
                        self.showPage('load');
                        //self.showResults = true;
                    }

                }
            });
        },
        //
        // loadResults: function () {
        //     this.questionIndex++;
        // },

        load: function (index) {
            var arr = this.userResults.filter(function(val) { return val });
            for (var i=0; i<arr.length; i ++) {
                if (parseInt(arr[i]['id']) === parseInt(index)) {
                    this.userResult = arr[i].id;
                    this.userResponses = JSON.parse(arr[i].answers);
                    this.goToResults();
                    break;
                }
            }
            this.setValues(this.userResponses);
        },



        /*openSaveForm: function () {
            this.showPage('save');
            this.showSaveForm = true;
        },*/
        openSuccess: function () {
            this.userResult = 0; // set user result to 0 after saving
            this.showPage('success');
            //this.showSuccess = true;
        },
        showMenu: function () {
            this.showEditMenu = !this.showEditMenu;
        },


        getParameterByName: function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },


        saveNotificationEmail: function () {
            var self = this;

            if (!self.notificationEmail) {
                self.showError('notification-email', 'Пожалуйста, введите свой email.');
                return;
            }

            if (self.notificationEmail && self.currentVariation && typeof self.currentVariation['sku_base_id'] !== "undefined") {
                var params = {
                    'email': self.notificationEmail,
                    'sku': self.currentVariation['sku_base_id'],
                    'prodbaseid': self.currentVariation['prod_base_id']
                };

                $.ajax({
                    url: '/notify',
                    data: params,
                    success: function (data) {
                        self.showNotificationModal = false;
                        self.showNotificationSuccessModal = true;
                    }
                });
            }
            else {
                alert('Ошибка! Попробуйте позже...');
            }

        },


        initOwl: function () {
            setTimeout(function () {
                if ($('.js-owl').length > 0) {
                    var owl = $('.js-owl');
                    if ($(window).width() < 641) {
                        if ($('.js-owl .owl-item').length > 0) {
                            $('.js-owl').html('');
                            owl.trigger('refresh.owl.carousel');
                            owl.trigger('destroy.owl.carousel');
                        }
                    }
                    else {

                        if ($('.js-owl .owl-item').length > 0) {
                            $('.js-owl').html('');
                            owl.trigger('refresh.owl.carousel');
                            owl.trigger('destroy.owl.carousel');
                        }

                        owl.owlCarousel({
                            margin: 0,
                            items: 1,
                            loop: false,
                            nav: true,
                            dots: false,
                            autoHeight: true,
                            //nestedItemSelector: '.owl-item',
                            //stageElement: 'div.js-owl',
                            navText: [
                                '<i class="fa fa-angle-left"></i>',
                                '<i class="fa fa-angle-right"></i>'
                            ],
                            autoWidth: false
                        });

                        $('.cl-card__owl-item').each(function (index) {
                            owl.trigger('add.owl.carousel', ['<div class="owl-item">' + $(this).html() + '</div>']).trigger('refresh.owl.carousel');
                        });
                    }
                }
            }, 400);
        }

    },
    watch: {
        isEnd: function (val, oldVal) {
            if (val === true) {
                this.initOwl();
            }
        },
        currentPage: function (val, oldVal) {
            if (oldVal !== val) {
                this.initOwl();
            }
        }
    },
});
