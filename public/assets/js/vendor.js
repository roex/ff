/**
 * Custom scripts
 */

// Required libraries
//= ../../bower_components/jquery/dist/jquery.min.js

// jQuery migrate
// ../../bower_components/jquery-migrate/jquery-migrate.min.js

// Bootstrap scripts
// ../../bower_components/bootstrap/js/tab.js
//= ../../bower_components/bootstrap/js/transition.js
//= ./partials/collapse.js
// ../../bower_components/bootstrap/js/tooltip.js
// ../../bower_components/bootstrap/js/modal.js
// ../../bower_components/bootstrap/js/dropdown.js


//= vendor/vue.js
//= ../../bower_components/owl.carousel/dist/owl.carousel.min.js
// vendor/vue.min.js
// ../../node_modules/vue-js-cookie/vue-js-cookie.js