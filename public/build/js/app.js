if (!Array.prototype.fill) {
    Object.defineProperty(Array.prototype, 'fill', {
        value: function(value) {

            // Steps 1-2.
            if (this == null) {
                throw new TypeError('this is null or not defined');
            }

            var O = Object(this);

            // Steps 3-5.
            var len = O.length >>> 0;

            // Steps 6-7.
            var start = arguments[1];
            var relativeStart = start >> 0;

            // Step 8.
            var k = relativeStart < 0 ?
                Math.max(len + relativeStart, 0) :
                Math.min(relativeStart, len);

            // Steps 9-10.
            var end = arguments[2];
            var relativeEnd = end === undefined ?
                len : end >> 0;

            // Step 11.
            var final = relativeEnd < 0 ?
                Math.max(len + relativeEnd, 0) :
                Math.min(relativeEnd, len);

            // Step 12.
            while (k < final) {
                O[k] = value;
                k++;
            }

            // Step 13.
            return O;
        }
    });
}


// components/test.js

// components/shareButtons.js

var radios = {
    template: '<div>\
    <h2 class="cl-test__title" v-html="question"></h2> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-wrapper\'"> \
        <div v-bind:class="\'cl-test__\'+prefix+\'-col\'" v-for="(option, index) in options">  \
            <div v-bind:class="\'cl-test__\'+prefix+\'-item\'">  \
                <input v-bind:name="id"  \
                       v-bind:value="option.value"  \
                       class="js-answer"\
                       v-bind:id="\'id-\' + id + \'-\' + option.id"\
                        :disabled="isDisabled(option.value)" \
                       v-on:click="updateValue($event.target.name, $event.target.value)"  \
                       type="radio"> \
                <label v-bind:for="\'id-\' + id + \'-\' + option.id" v-html="option.text"></label>  \
            </div>  \
        </div>  \
        </div>  \
    </div>',
    props: ['id', 'question', 'options', 'prefix'],
    components: {},
    methods: {
        isDisabled: function(value) {
            var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                totalQuestions = this.$parent.quiz.questions.length;

            if (totalAnswered === totalQuestions)
                return false;

            if (totalAnswered >= 1) {
                var questionIndex = this.id + 1,
                    allVariationsInQuestion = [];

                var availableVariations = this.$parent.availableVariations;
                for (var i=0; i<availableVariations.length; i++) {
                    var result = availableVariations[i];
                    if (result['question_' + questionIndex] instanceof Array) {
                        allVariationsInQuestion = allVariationsInQuestion.concat(result['question_' + questionIndex]);
                    }
                    else {
                        allVariationsInQuestion.push(parseInt(result['question_' + questionIndex]));
                    }
                }


                if (allVariationsInQuestion.indexOf(parseInt(value)) > -1)
                    return false;
                else
                    return true;

            }
        },
        updateValue: function (name, value) {
            // var res = this.$parent.userResponses[name].filter(function(val) { return val }),
            //     isEdit = false;

            res = [];
            res.push(value);
            this.$emit('input', res);

            if (this.$parent.userResponses[name] !== null) {
                var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                    totalQuestions = this.$parent.quiz.questions.length;
                if (totalAnswered === totalQuestions) {
                    this.$emit('edit');
                }
                else {
                    this.$emit('change');
                }
            }
            else {
                this.$emit('answer');
            }

        },
    }
};

var checkboxes = {
    template: '<div> \
    <h2 class="cl-test__title" v-html="question"></h2> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-wrapper\'"> \
    <div v-bind:class="\'cl-test__\'+prefix+\'-col\'" v-for="(option, index) in options"> \
        <div v-bind:class="\'cl-test__\'+prefix+\'-item\'"> \
            <input v-bind:name="id"  \
                   v-bind:value="option.value"  \
                   v-bind:id="\'id-\' + id + \'-\' + option.id" \
                   :disabled="isDisabled(option.value)" \
                   class="js-answer" \
                   v-on:click="updateValue($event.target.name, $event.target.value, $event.target.checked)"  \
                   type="checkbox"> \
          <label v-bind:for="\'id-\' + id + \'-\' + option.id" v-html="option.text"></label>  \
        </div>  \
    </div>  \
    </div>  \
    <a href="javascript:;" v-show="isAnswered" v-on:click="finish" class="cl-test__link-skip">К результатам <i class="ico"></i></a>\
    <a href="javascript:;" v-show="isNotAnswered" v-on:click="skip" class="cl-test__link-skip">Вы можете пропустить этот шаг <i class="ico"></i></a>\
    </div>',
    props: ['id', 'question', 'options', 'prefix'],
    computed: {
        isNotAnswered: function () {
            var tempArr = this.$parent.userResponses[this.id];
            if (tempArr === null) {
                return true;
            }
            else {
                if (tempArr.filter(function(val) { return val }).length === 0) {
                    return true;
                }
            }
            return false;
        },
        isAnswered: function() {
            return this.$parent.userResponses[this.id] !== null && this.$parent.userResponses[this.id].filter(function(val) { return val }).length  > 0;
        },
    },
    methods: {
        isDisabled: function(value) {

            //console.log(value);
            //console.log(this);

            var totalAnswered = this.$parent.userResponses.filter(function(val) { return val }).length,
                //totalQuestions = this.$parent.quiz.questions.length,
                inResults = false;

            // if (totalAnswered === totalQuestions)
            //     return true;

            if (totalAnswered >= 1) {
                var questionIndex = this.id + 1,
                    allVariationsInQuestion = [];

                var availableVariations = this.$parent.availableVariations;
                for (var i = 0; i < availableVariations.length; i++) {
                    var result = availableVariations[i];
                    if (result['question_' + questionIndex] instanceof Array) {
                        allVariationsInQuestion = allVariationsInQuestion.concat(result['question_' + questionIndex]);
                    }
                    else {
                        allVariationsInQuestion.push(parseInt(result['question_' + questionIndex]));
                    }
                }

                inResults = allVariationsInQuestion.indexOf(parseInt(value)) > -1;

            }

            // Если тест пройден
            if (this.$parent.isEnd) {
                // Считаем сколько элементов в массиве и бликруем если больше нужного
                if (this.$parent.userResponses[this.id] !== null) {
                    var tempArr = this.$parent.userResponses[this.id].filter(function(val) { return val });
                    return tempArr.length >= 2 && tempArr.indexOf(value+'') === -1;
                }
                else {
                    return false;
                }
            }
            else {
                if (inResults) {
                    // Считаем сколько элементов в массиве и бликруем если больше нужного
                    if (this.$parent.userResponses[this.id] !== null) {
                        var tempArr = this.$parent.userResponses[this.id].filter(function(val) { return val });
                        return tempArr.length >= 2 && tempArr.indexOf(value+'') === -1;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }


        },
        updateValue: function (name, value, checked) {
            var userResponses = this.$parent.userResponses,
                res = userResponses[name] === null ? [] : userResponses[name].filter(function(val) { return val });

            if (checked) {
                res.push(value);
            } else {
                var index = res.indexOf(value);
                if (index > -1)
                    res.splice(index, 1);
            }

            this.$emit('input', res);
        },
        skip: function () {
            this.$parent.isEnd = true;
            this.$emit('skip');
            this.$emit('finish');
        },
        finish: function () {
            this.$parent.isEnd = true;
            this.$emit('finish');
        }
    }
};


// Create a quiz object with a title and two questions.
// A question has one or more answer, and one or more is valid.
var quiz = {
    title: 'CL Foundation Finder',
    questions: [
        {
            id: 0,
            class: 'shade',
            classes: '',
            type: 'radios',
            category: 'Тон',
            answers: [
                'Очень светлый',
                'Очень светлый',
                'Очень светлый',
                'Очень светлый',
                'Очень светлый',
                'Светлый',
                'Светлый',
                'Светлый',
                'Светлый',
                'Светлый',
                'Средний',
                'Средний',
                'Средний',
                'Средний',
                'Средний'
            ],
            text: 'Шаг 1. Какой оттенок кожи наиболее близок к вашему?',
            responses: [
                {id: 1, text: '<img src="build/img/madels-1.png">', value: 1, attributes: {}},
                {id: 2, text: '<img src="build/img/madels-2.png">', value: 2, attributes: {}},
                {id: 3, text: '<img src="build/img/madels-3.png">', value: 3, attributes: {}},
                {id: 4, text: '<img src="build/img/madels-4.png">', value: 4, attributes: {}},
                {id: 5, text: '<img src="build/img/madels-5.png">', value: 5, attributes: {}},
                {id: 6, text: '<img src="build/img/madels-6.png">', value: 6, attributes: {}},
                {id: 7, text: '<img src="build/img/madels-7.png">', value: 7, attributes: {}},
                {id: 8, text: '<img src="build/img/madels-8.png">', value: 8, attributes: {}},
                {id: 9, text: '<img src="build/img/madels-9.png">', value: 9, attributes: {}},
                {id: 10, text: '<img src="build/img/madels-10.png">', value: 10, attributes: {}},
                {id: 11, text: '<img src="build/img/madels-11.png">', value: 11, attributes: {}},
                {id: 12, text: '<img src="build/img/madels-12.png">', value: 12, attributes: {}},
                {id: 13, text: '<img src="build/img/madels-13.png">', value: 13, attributes: {}},
                {id: 14, text: '<img src="build/img/madels-14.png">', value: 14, attributes: {}},
                {id: 15, text: '<img src="build/img/madels-15.png">', value: 15, attributes: {}},
            ],
            prefix: '',
            suffix: '',
            skip: false,
        },
        {
            id: 1,
            type: 'radios',
            class: 'sub-shade',
            classes: '',
            category: 'Подтон',
            answers: [
                'Холодный',
                'Теплый',
            ],
            text: 'Шаг 2. Какой у вас подтон кожи?',
            responses: [
                {id: 16, text: '<div class="cold"><div class="cl-test__sub-shade-dt"><div class="cl-test__sub-shade-dtc"><span class="title"><i></i>Холодный</span><span>Ближе к розовому, чем к желтому.</span></div></div></div>', value: 1, attributes: {comment: ''}},
                {id: 17, text: '<div class="warm"><div class="cl-test__sub-shade-dt"><div class="cl-test__sub-shade-dtc"><span class="title"><i></i>Теплый</span><span>Ближе к желтому, чем к розовому.</span></div></div></div>',   value: 2, attributes: {comment: ''}},
            ],
            prefix: '',
            suffix: '',
            //suffix: 'Сложно определиться? <a href="http://livehelper.itmolcom.com/index.php/rus/chat/chatwidget/(department)/1/2/(theme)/4/(vid)/okzeco5jxbkkcz4ssn9?URLReferer=%2F%2Fwww.clinique.ru%2F&amp;dt=%D0%9E%D1%84%D0%B8%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%20Clinique">Попробуем вместе</a> / Хотите разобраться детальнее? <a href="http://livehelper.itmolcom.com/index.php/rus/chat/chatwidget/(department)/1/2/(theme)/4/(vid)/okzeco5jxbkkcz4ssn9?URLReferer=%2F%2Fwww.clinique.ru%2F&amp;dt=%D0%9E%D1%84%D0%B8%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%20Clinique">Вам сюда</a>',
            skip: true,
        },
        {
            id: 2,
            type: 'radios',
            class: 'coating-density',
            classes: '',
            category: 'Покрытие',
            answers: [
                'Легкое',
                'Среднее',
                'Плотное'
            ],
            text: 'Шаг 3. Какую плотность покрытия вы предпочитаете?',
            responses: [
                {id: 18, text: '<span class="title"><i></i>Легкую</span><img src="build/img/coating-density-1.png">',  value: 1, attributes: {}},
                {id: 19, text: '<span class="title"><i></i>Среднюю</span><img src="build/img/coating-density-2.png">', value: 2, attributes: {}},
                {id: 20, text: '<span class="title"><i></i>Плотную</span><img src="build/img/coating-density-3.png">', value: 3, attributes: {}},
            ],
            prefix: '',
            suffix: '',
            skip: false,
        },
        {
            id: 3,
            type: 'radios',
            class: 'texture',
            classes: '',
            category: 'Текстура',
            answers: [
                'Жидкая формула',
                'Пудра',
                'BB / CC Крем',
                'Стик'
            ],
            text: 'Шаг 4. Какую текстуру <br> тонального средства вы ищите?',
            responses: [
                {id: 21, text: '<span class="title"><i></i>Жидкая формула</span><img src="build/img/texture-1.png">', value: 1, attributes: {}},
                {id: 22, text: '<span class="title"><i></i>Пудра</span><img src="build/img/texture-2.png">',          value: 2, attributes: {}},
                {id: 23, text: '<span class="title"><i></i>BB / CC Крем</span><img src="build/img/texture-3.png">',   value: 3, attributes: {}},
                {id: 24, text: '<span class="title"><i></i>Стик</span><img src="build/img/texture-4.png">',           value: 4, attributes: {}},
            ],
            prefix: 'Мы уже прошли половину!',
            suffix: '',
            skip: false,
        },
        {
            id: 4,
            type: 'radios',
            class: 'effect',
            classes: '',
            category: 'Эффект',
            answers: [
                'Матовый',
                'Естественный матовый',
                'Естественный',
                'Сияющий',
            ],
            text: 'Шаг 5. Какой эффект <br> вы хотите получить?',
            responses: [
                {id: 25, text: '<span class="title"><i></i>Матовый <span>Без жирного блеска</span></span><img src="build/img/effect-1.png">',                             value: 1, attributes: {}},
                {id: 26, text: '<span class="title"><i></i>Естественный матовый <span>Бархатная кожа без жирного блеска</span></span><img src="build/img/effect-2.png">', value: 2, attributes: {}},
                {id: 27, text: '<span class="title"><i></i>Естественный <span>Естественное сияние здоровой кожи</span></span><img src="build/img/effect-3.png">',         value: 3, attributes: {}},
                {id: 28, text: '<span class="title"><i></i>Сияющий <span>Увлажненная сияющая кожа</span></span><img src="build/img/effect-4.png">',                       value: 4, attributes: {}},
            ],
            prefix: '',
            suffix: '',
            skip: false,
        },
        {
            id: 5,
            type: 'checkboxes',
            class: 'problem',
            classes: '',
            category: 'Уход',
            answers: [
                'Выровнять тон кожи',
                'Убрать покраснения',
                'Убрать жирный блеск',
                'Дополнительно увлажнить',
                'Разгладить морщины',
                'Защитить от солнца',
                'Убрать несовершенства кожи',
            ],
            text: 'Шаг 6. Какую проблему кожи вы хотите решить?',
            responses: [
                {id: 29, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43" height="43" viewBox="0 0 43 43"><defs><path id="a1" d="M207.66 206.03A21.5 21.5 0 0 1 229 227.51 21.5 21.5 0 0 1 207.72 249h-.38A21.51 21.51 0 0 1 186 227.51a21.5 21.5 0 0 1 21.29-21.48h.02l.11-.03.24.03zm-19.94 21.48a19.75 19.75 0 0 0 18.92 19.74v-39.48h-.16a19.76 19.76 0 0 0-18.76 19.74zm20.64 19.74c2.16-.09 4.23-.53 6.15-1.26a.9.9 0 0 1-.13-.43c0-.47.39-.86.86-.86.4 0 .71.28.81.65a19.77 19.77 0 0 0 9.29-9.3.84.84 0 0 1-.64-.8c0-.47.39-.86.86-.86.16 0 .3.06.43.13a19.65 19.65 0 0 0 0-14.02.81.81 0 0 1-.43.14.87.87 0 0 1-.86-.86c0-.4.28-.71.64-.81a19.75 19.75 0 0 0-9.29-9.29.85.85 0 0 1-1.67-.21c0-.17.06-.31.13-.43a19.59 19.59 0 0 0-6.15-1.27zm2.58-37.78c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm2.58 2.57a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm-7.74 2.58a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm-7.74 2.58c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm-12.9 2.58c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm-7.74 2.58c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm5.16 0c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm5.16 0c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm-12.9 2.58c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm5.16 0c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm5.16 0c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm5.16 0c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm-12.9 2.57a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm-12.9 2.58a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm-12.9 2.58c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm-12.9 2.58c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm5.16 0c0 .47-.39.86-.86.86a.87.87 0 0 1-.86-.86c0-.47.39-.86.86-.86s.86.39.86.86zm-7.74 2.58c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm5.16 0c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm5.16 0c0 .47-.39.86-.86.86a.86.86 0 1 1 .86-.86zm-12.9 2.58c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm5.16 0c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm5.16 0c0 .47-.39.85-.86.85a.86.86 0 0 1-.86-.85.86.86 0 1 1 1.72 0zm-7.74 2.57a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm5.16 0a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0zm-7.74 2.58a.86.86 0 1 1-1.72 0 .86.86 0 0 1 1.72 0z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-186 -206)"><use xlink:href="#a1"/></g></svg></i>Выровнять тон кожи</span></div></div>',             value: 1, attributes: {}},
                {id: 30, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40" viewBox="0 0 40 40"><defs><path id="a2" d="M576 226a20 20 0 1 1-40.01-.01A20 20 0 0 1 576 226zm-38.26 0c0 10.1 8.16 18.26 18.26 18.26 10.1 0 18.26-8.16 18.26-18.26 0-10.1-8.16-18.26-18.26-18.26A18.24 18.24 0 0 0 537.74 226zm26.09 0a7.85 7.85 0 0 1-7.83 7.83 7.85 7.85 0 0 1-7.83-7.83 7.85 7.85 0 0 1 7.83-7.83 7.85 7.85 0 0 1 7.83 7.83zm-13.92 0a6.08 6.08 0 1 0 12.16.02 6.08 6.08 0 0 0-12.16-.02z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-536 -206)"><use xlink:href="#a2"/></g></svg></i>Убрать покраснения</span></div></div>', value: 2, attributes: {}},
                {id: 31, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="44" height="44" viewBox="0 0 44 44"><defs><path id="a3" d="M199.51 284.24l25.25 7.82A5.01 5.01 0 0 1 229 297v26c0 2.74-2.25 5-5 5h-27c-2.75 0-5-2.26-5-5v-3.29l-3.53-1.21h-.01a5.02 5.02 0 0 1-3.22-6.29l7.98-24.74a5 5 0 0 1 6.29-3.23zm-4.39 3.84l-1.79 5.55c.91-1 2.21-1.63 3.67-1.63h20.79l-18.89-5.85-.01-.01a3 3 0 0 0-3.77 1.94zM194 297v26c0 1.66 1.33 3 3 3h27c1.67 0 3-1.34 3-3v-26c0-1.33-.85-2.42-2.02-2.83-.11 0-.22-.02-.33-.05l-.28-.09-.37-.03h-27c-1.67 0-3 1.33-3 3zm31 1a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm-37.85 14.83a2.97 2.97 0 0 0 1.95 3.78l2.9.98v-19.83zm14.8-11.15c1.45-1.12 4.95-3.68 4.95-3.68s-2.57 3.49-3.69 4.95c1.12 1.45 3.69 4.95 3.69 4.95s-3.5-2.57-4.95-3.69c-1.46 1.12-4.95 3.69-4.95 3.69s2.56-3.5 3.69-4.95C199.56 301.49 197 298 197 298s3.49 2.56 4.95 3.68zm4.05 10.16c1.09-.72 3-1.84 3-1.84s-1.1 1.93-1.8 3.04c.7 1.07 1.8 2.96 1.8 2.96s-1.91-1.07-3-1.76c-1.09.69-3 1.76-3 1.76s1.1-1.89 1.79-2.96C204.1 311.93 203 310 203 310s1.91 1.12 3 1.84z"/></defs><desc>Generated with Avocode.</desc><g transform="translate(-185 -284)"><use fill="#5dba98" xlink:href="#a3"/></g></svg></i>Убрать жирный блеск</span></div></div>',      value: 3, attributes: {}},
                {id: 32, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="31" height="44" viewBox="0 0 31 44"><defs><path id="a4" d="M556.19 283.33s3.68 4.5 7.36 10.21c3.68 5.71 7.45 12.57 7.45 17.9 0 8.58-6.95 15.56-15.5 15.56s-15.5-6.98-15.5-15.56c0-5.33 3.77-12.19 7.45-17.9a124.96 124.96 0 0 1 7.36-10.21.9.9 0 0 1 .5-.3l.11-.03c.29-.02.58.1.77.33zm-7.26 11.15c-3.62 5.62-7.17 12.51-7.17 16.96a13.75 13.75 0 1 0 27.48 0c0-4.45-3.55-11.34-7.17-16.96a105.48 105.48 0 0 0-6.57-9.13c-.86 1.07-3.5 4.36-6.57 9.13zm6.82-1.37a.9.9 0 0 1-.09 1.01s-2.08 2.7-4.17 6.23c-2.1 3.54-4.12 7.95-4.12 10.93 0 4.14 1.8 6.32 3.62 7.58 1.82 1.27 3.63 1.52 3.63 1.52a.87.87 0 0 1 .6 1.45.85.85 0 0 1-.87.29s-2.2-.31-4.37-1.82c-2.18-1.52-4.37-4.35-4.37-9.02 0-3.73 2.21-8.17 4.37-11.81 2.15-3.64 4.31-6.43 4.31-6.43a.88.88 0 0 1 .55-.35c.36-.07.73.11.91.42z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-540 -283)"><use xlink:href="#a4"/></g></svg></i>Дополнительно увлажнить</span></div></div>',        value: 4, attributes: {}},
                {id: 33, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="41" height="31" viewBox="0 0 41 31"><defs><path id="a5" d="M192.67 369.01c.34-.03.67.15.82.46 0 0 1.43 2.8 4.18 2.8s4.17-2.8 4.17-2.8a.83.83 0 0 1 1.49 0s1.42 2.8 4.17 2.8 4.17-2.8 4.17-2.8a.83.83 0 0 1 1.49 0s1.42 2.8 4.17 2.8c2.76 0 4.18-2.8 4.18-2.8a.8.8 0 0 1 .74-.47.8.8 0 0 1 .74.47s1.46 2.8 4.18 2.8c.29 0 .57.15.72.41a.8.8 0 0 1 0 .82.83.83 0 0 1-.72.41 5.68 5.68 0 0 1-4.92-2.73 5.62 5.62 0 0 1-4.92 2.73 5.62 5.62 0 0 1-4.91-2.73 5.64 5.64 0 0 1-4.92 2.73 5.64 5.64 0 0 1-4.92-2.73 5.62 5.62 0 0 1-4.91 2.73 5.62 5.62 0 0 1-4.92-2.73 5.68 5.68 0 0 1-4.92 2.73.83.83 0 0 1-.72-.41.8.8 0 0 1 0-.82.83.83 0 0 1 .72-.41c2.71 0 4.18-2.8 4.18-2.8a.82.82 0 0 1 .58-.46h.08zm0 13.05c.34-.04.67.15.82.46 0 0 1.43 2.8 4.18 2.8s4.17-2.8 4.17-2.8a.83.83 0 0 1 1.49 0s1.42 2.8 4.17 2.8 4.17-2.8 4.17-2.8a.83.83 0 0 1 1.49 0s1.42 2.8 4.17 2.8c2.76 0 4.18-2.8 4.18-2.8a.8.8 0 0 1 .74-.47.8.8 0 0 1 .74.47s1.46 2.8 4.18 2.8c.29 0 .57.15.72.41a.8.8 0 0 1 0 .82.82.82 0 0 1-.72.4 5.68 5.68 0 0 1-4.92-2.72 5.64 5.64 0 0 1-4.92 2.72 5.63 5.63 0 0 1-4.91-2.72 5.65 5.65 0 0 1-4.92 2.72 5.65 5.65 0 0 1-4.92-2.72 5.63 5.63 0 0 1-4.91 2.72 5.64 5.64 0 0 1-4.92-2.72 5.68 5.68 0 0 1-4.92 2.72.82.82 0 0 1-.72-.4.8.8 0 0 1 0-.82.83.83 0 0 1 .72-.41c2.71 0 4.18-2.8 4.18-2.8a.82.82 0 0 1 .58-.46h.08zm0 13.05c.34-.04.67.15.82.46 0 0 1.43 2.8 4.18 2.8s4.17-2.8 4.17-2.8a.82.82 0 0 1 .74-.48c.32 0 .61.19.75.48 0 0 1.42 2.8 4.17 2.8s4.17-2.8 4.17-2.8a.84.84 0 0 1 .75-.48c.31 0 .61.19.74.48 0 0 1.42 2.8 4.17 2.8 2.76 0 4.18-2.8 4.18-2.8a.81.81 0 0 1 1.48 0s1.46 2.8 4.18 2.8c.29 0 .57.15.72.4.15.26.15.57 0 .83a.84.84 0 0 1-.72.4 5.66 5.66 0 0 1-4.92-2.73 5.62 5.62 0 0 1-4.92 2.73 5.62 5.62 0 0 1-4.91-2.73 5.64 5.64 0 0 1-4.92 2.73 5.64 5.64 0 0 1-4.92-2.73 5.62 5.62 0 0 1-4.91 2.73 5.62 5.62 0 0 1-4.92-2.73 5.66 5.66 0 0 1-4.92 2.73.84.84 0 0 1-.72-.4.82.82 0 0 1 0-.83c.15-.25.43-.4.72-.4 2.71 0 4.18-2.8 4.18-2.8a.82.82 0 0 1 .58-.46h.08z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-187 -369)"><use xlink:href="#a5"/></g></svg></i>Разгладить морщины</span></div></div>', value: 5, attributes: {}},
                {id: 34, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43" height="43" viewBox="0 0 43 43"><defs><path id="a" d="M556.84 361.27c.21.2.32.48.31.77v6c0 .36-.18.69-.5.87a1 1 0 0 1-1 0 .98.98 0 0 1-.5-.87v-6a1 1 0 0 1 .78-1 .5.5 0 0 1 .13-.04c.28-.02.57.07.78.27zm-14.94 5.61a1 1 0 0 1 .81.31l4.25 4.25a1 1 0 0 1 .35 1.01c-.09.37-.38.66-.75.75a1 1 0 0 1-1-.35l-4.25-4.25a1 1 0 0 1-.28-1.02c.1-.36.4-.63.78-.7h.09zm29.31.57c.19.39.1.85-.22 1.15l-4.25 4.25a1 1 0 0 1-1 .35 1.02 1.02 0 0 1-.75-.75 1 1 0 0 1 .35-1.01l4.25-4.25a.98.98 0 0 1 1.62.26zm-15.06 4.59h.06a.9.9 0 0 1 .32.06c.03 0 .05.03.09.03a9.97 9.97 0 0 1 9.53 9.91c0 5.5-4.5 10-10 10s-10-4.5-10-10a9.98 9.98 0 0 1 9.59-9.91l.04-.03a.57.57 0 0 1 .18-.03l.1-.03h.09zm-8 10a8 8 0 1 0 8.09-8h-.15a8 8 0 0 0-7.94 8zm-12.19-1H542.15c.36-.01.7.18.88.49.18.31.18.7 0 1.01-.18.31-.52.5-.88.5h-6a1.02 1.02 0 0 1-1.14-.86 1 1 0 0 1 .86-1.14h.09zm34 0H576.15c.36-.01.7.18.88.49.18.31.18.7 0 1.01-.18.31-.52.5-.88.5h-6a1.02 1.02 0 0 1-1.14-.86 1 1 0 0 1 .86-1.14h.09zm-23.72 9.87c.41-.01.79.24.95.61a1 1 0 0 1-.23 1.11l-4.25 4.25a1 1 0 0 1-1 .35 1 1 0 0 1-.4-1.76l4.25-4.25a1 1 0 0 1 .59-.31h.09zm19.69 0h.1c.27 0 .53.11.71.31l4.25 4.25a1 1 0 0 1-.4 1.76 1 1 0 0 1-1-.35l-4.25-4.25a1 1 0 0 1-.28-1.02c.1-.36.4-.63.78-.7h.09zm-9.09 4.36c.21.2.32.48.31.77v6c0 .36-.18.69-.5.87a1 1 0 0 1-1 0 .98.98 0 0 1-.5-.87v-6a1 1 0 0 1 .78-1 .5.5 0 0 1 .13-.04c.28-.02.57.07.78.27z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-534 -360)"><use xlink:href="#a"/></g></svg></i>Защитить от солнца</span></div></div>',       value: 6, attributes: {}},
                {id: 35, text: '<div class="cl-test__problem-dt"><div class="cl-test__problem-dtc"><span class="title"><i class="cl-test__problem-ico"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="44" height="44" viewBox="0 0 44 44"><defs><path id="a7" d="M228.21 441.95h.02a3.19 3.19 0 0 1 .15 4.21l-.03.03-14.06 16.42a.9.9 0 0 1-.23.47l-9.31 11.03-.09.12-4.54 8.19-.03.08-.03-.02a2.58 2.58 0 0 1-2.22 1.54v.03h-.08c-.67.05-1.9 0-3.48-.4a10.8 10.8 0 0 1-5.05-2.75c-3.11-3.11-3.23-7.74-3.23-8.61v-.14c.13-.85.64-1.73 1.57-2.16l-.02-.03.08-.03 8.05-4.46c.07-.1.15-.19.26-.25l11.14-9.26a.73.73 0 0 1 .31-.17l16.52-13.98.03-.02a3.22 3.22 0 0 1 2.19-.79c.78.02 1.51.33 2.08.95zm-3.09 1.18l-.03.03-16.11 13.61 4.3 4.32 13.69-16.05c.45-.55.43-1.39-.03-1.85l-.02-.03c-.3-.33-.7-.47-1.1-.39-.24.04-.49.17-.7.36zm-27.28 22.85l6.21 6.2 8.13-9.63-4.6-4.66zm-9.42 5.55l-.09.06c-.27.11-.47.41-.53.78v.03c0 .27.06 1.05.25 2.05l3.76-2.39c.1-.06.2-.11.31-.14a.92.92 0 0 1 1.04.55.9.9 0 0 1-.39 1.11l-4.24 2.69c.4 1.18 1 2.41 1.96 3.37a8.5 8.5 0 0 0 2.89 1.83l1.15-1.55a.9.9 0 0 1 1.49.05.9.9 0 0 1-.06 1.02l-.75 1.01c1.18.24 2.18.28 2.47.25h.08c.2 0 .57-.23.7-.53l.06-.09 4.43-8.02-6.51-6.51z"/></defs><desc>Generated with Avocode.</desc><g  transform="translate(-185 -441)"><use xlink:href="#a7"/></g></svg></i>Убрать несовершенства кожи</span></div></div>',   value: 7, attributes: {}},
            ],
            prefix: '',
            suffix: '',
            skip: false,
        }
    ],
    /*questions2: [
        {
            id: 0,
            class: 'shade',
            classes: '',
            type: 'radios',
            category: 'Тип кожи',
            text: 'Шаг 1. Какой оттенок кожи наиболее близок к вашему?',
            responses: [
                {id: 1, text: '<img src="build/img/madels-1.png">', value: 1, attributes: {}},
                {id: 2, text: '<img src="build/img/madels-2.png">', value: 2, attributes: {}},
                {id: 3, text: '<img src="build/img/madels-3.png">', value: 3, attributes: {}},
                {id: 4, text: '<img src="build/img/madels-4.png">', value: 4, attributes: {}},
                {id: 5, text: '<img src="build/img/madels-5.png">', value: 5, attributes: {}},
                {id: 6, text: '<img src="build/img/madels-6.png">', value: 6, attributes: {}},
                {id: 7, text: '<img src="build/img/madels-7.png">', value: 7, attributes: {}},
                {id: 8, text: '<img src="build/img/madels-8.png">', value: 8, attributes: {}},
                {id: 9, text: '<img src="build/img/madels-9.png">', value: 9, attributes: {}},
                {id: 10, text: '<img src="build/img/madels-10.png">', value: 10, attributes: {}},
                {id: 11, text: '<img src="build/img/madels-11.png">', value: 11, attributes: {}},
                {id: 12, text: '<img src="build/img/madels-12.png">', value: 12, attributes: {}},
                {id: 13, text: '<img src="build/img/madels-13.png">', value: 13, attributes: {}},
                {id: 14, text: '<img src="build/img/madels-14.png">', value: 14, attributes: {}},
                {id: 15, text: '<img src="build/img/madels-15.png">', value: 15, attributes: {}},
            ],
            prefix: '',
            suffix: '',
            skip: false,
        },
    ]*/
};

// register modal component
Vue.component('load', {
    template: '#page-load',
    data: function () {
        return {
            results: this.$parent.userResults
        }
    },
    methods: {
        load: function (index) {
            var arr = this.$parent.userResults.filter(function(val) { return val });
            for (var i=0; i<arr.length; i ++) {
                if (parseInt(arr[i]['id']) === parseInt(index)) {
                    this.$parent.userResult = JSON.parse(arr[i].answers);
                    break;
                }
            }
            this.$emit('loaded');
            //this.$emit('close');
        }
    }
});


Vue.component('save', {
    template: '#page-save',
    data: function () {
        return {
            agreeWithProcessing: true,
            wantReceiveInformation: true,
            saveEmail: ''
        }
    },
    /*computed: {
        isEmailEmpty: function () {
            return this.agreeWithProcessing;
        }
    },*/
    methods: {

        goToResults: function () {
            this.$parent.showPage('results');
        },

        save: function () {

            var self = this;

            var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
            if (this.saveEmail == '' || !re.test(this.saveEmail)) {
                alert('Введите email!');
            }
            else {

                var params = {
                    id: this.$parent.userResult,
                    pbid: parseInt(this.$parent.results.bigProduct.id),
                    total: 1 + this.$parent.results.smallProducts.length,
                    link: encodeURI(this.$parent.results.bigProduct.link),
                    image: encodeURI(this.$parent.results.bigProduct.image),
                    email: this.saveEmail,
                    answers: JSON.stringify(this.$parent.userResponses),
                    subscriber: this.wantReceiveInformation
                };

                $.ajax({
                    url: '/save',
                    data: params,
                    success: function (data) {
                        // test@mail.ru
                        //self.$emit('close');
                        self.$emit('success');
                    }
                });

            }

        },
    }
});


// register modal component
Vue.component('success', {
    template: '#page-success',
    methods: {
        goToResults: function () {
            this.$parent.showPage('results');
            //this.$parent.showSuccess = false;
            // this.$parent.showSaveForm = false;
        },
    }
});
// components/modal.js


new Vue({
    el: '#app',
    data: {
        // Store current question index
        questionIndex: -1,
        isEnd: false,

        // Main data
        quiz: quiz,

        // Prepare results
        results: {
            'bigProduct': {
                'id': 0,
                'title': '',
                'image': '',
                'link': '',
                'description': '',
                'weight': 0,
                'price': 0,
                'lh1': '',
                'lh2': '',
                'review1': '',
                'review2': '',
                'variations': [],
                'variationsAll': [],
                'variationsMain': [],
            }, // with variations
            'smallProducts': [], // without variations
            'lifehack': 0,
            'empty': {
                'ids': [
                    9285,
                    6201,
                    42938,
                    5276,
                    39658,
                    45511
                ],
                'filtered': []
            },
        },

        // An array initialized with "null" values for each question
        // It means: "did the user answered correctly to the question n?" "no".
        userResponses: Array(quiz.questions.length).fill(null),

        // Used for loading results
        userEmail: '',
        // Used for notifications
        notificationEmail: '',

        // Store current page
        currentPage: 'quiz',

        // Used for page with results (buy button, variation title, etc.)
        currentVariation: null,

        // Show/hide some elements
        showModal: false, // show modal window with variations
        showNotificationModal: false, // show modal window with notification (when product is not available)
        showNotificationSuccessModal: false, // show success modal window
        showEditMenu: false, // show menu on page with results

        //showResults: false,
        //showSaveForm: false,
        //showSuccess: false,

        // Последние 3 результата пользователя
        userResults: {},
        // Если пользователь выбрал конкретный резултат, то сохраняем его ID здесь
        userResult: 0,


        //saveEmail: '',
        //agreeWithProcessing: false,
        //wantReceiveInformation: false,

        // Этот параметр используется для шаринга
        //result: 2,

        // Products from feed
        products: {},
        //productsTree: {},

        productsFromDB: {},

        // Available results from db
        availableResults: {},

        // Filtered available results from db
        availableVariations: [],
        availableVariationsTree: {},
        //availableProducts: [],
        //bigProduct: null,
    },
    created: function () {

        var self = this;

        //TODO We should use when

        // load available results
        $.get('/variations', function(data) {
            if (data && data['results']) {
                var res = data['results'];
                // convert string to array
                for (var i = 0; i < res.length; i++) {
                    for (var property in res[i]) {
                        if (res[i].hasOwnProperty(property) && (res[i][property]+'').indexOf('[') > -1) {
                            res[i][property] = JSON.parse(res[i][property]);
                        }
                    }
                }
                self.availableResults = data['results'];
                for (var i = 0; i < self.availableResults.length; i++) {
                    var variation = self.availableResults[i];
                    if (typeof self.availableVariationsTree[variation['prod_base_id']] === "undefined")
                        self.availableVariationsTree[variation['prod_base_id']] = [];

                    self.availableVariationsTree[variation['prod_base_id']].push(variation);
                }
            }
        }, 'json');

        // load available products from db
        $.get('/products', function(data) {
            if (data && data['results']) {
                for (var i = 0; i < data['results'].length; i++) {
                    var id = data['results'][i]['prod_base_id'];
                    self.productsFromDB[id] = data['results'][i];
                    self.productsFromDB[id]['id'] = id;
                    self.productsFromDB[id]['price'] = self.price(data['results'][i]['price']);
                }
            }
        }, 'json');

        // load products from feed
        $.get('/feed', function(data) {
            if (data && data['products']) {
                self.products = data['products'];

                /*for (var i = 0; i < self.products.length; i++) {
                    var product = self.products[i],
                        id = product['g:prodbaseID'][0];

                    if (typeof self.productsTree[id] === "undefined") {
                        //console.log(product);
                        self.productsTree[id] = {
                            'id': product['g:prodbaseID'][0],
                            'title': product['title'][0],
                            'description': product['description'][0],
                            'image': product['g:image_link'][0],
                            'link': product['link'][0],
                            'size': typeof product['g:size'] !== "undefined" ? product['g:size'][0] : '',
                            'price': self.price(product['g:price'][0]),
                            'variations': [],
                            'variationsAll': [],
                            'variationsMain': [],
                            'variationsMore': [],
                        };
                    }

                    self.productsTree[id]['variations'].push(product);
                }*/

                $('body').addClass('loaded');

                var isShare = self.getParameterByName('share');
                if (parseInt(isShare) === 1) {
                    self.userResult = -1;
                    self.userResponses = [
                        [self.getParameterByName('q1')],
                        [self.getParameterByName('q2')],
                        [self.getParameterByName('q3')],
                        [self.getParameterByName('q4')],
                        [self.getParameterByName('q5')],
                        self.getParameterByName('q6') && self.getParameterByName('q6') !== '' ? self.getParameterByName('q6').split(' ') : [],
                    ];
                    self.goToResults();
                    self.setValues(self.userResponses);
                }
            }
        }, 'json');

    },
    // The view will trigger these methods on click
    components: {
        'checkboxes': checkboxes,
        'radios': radios,
        //'test': test
    },
    methods: {
        // Go to next question
        next: function() {
            this.questionIndex++;
        },
        begin: function () {
            this.showPage('quiz');
            this.next();
        },
        // Go to the first question (play again)
        again: function () {
            this.showPage('quiz');
            // Uncheck all radios and checkboxes
            var inputs = document.getElementsByClassName('js-answer');
            for(var i = 0; i<inputs.length; i++){
                inputs[i].checked = false;
            }
            this.userResponses = Array(quiz.questions.length).fill(null);
            this.questionIndex = 0;
            this.userResult = 0;
            this.isEnd = false;
            this.clearPrevResults();
            //this.results.bigProduct['id'] = 0;
            this.showPage('quiz');
            //$('.collapse').collapse('hide');
        },
        // Go to question
        go: function (questionNumber) {
            var self = this;
            this.questionIndex = questionNumber;
            this.findResults();
            this.showPage('quiz');
        },
        // Skip the question
        skip: function() {
            this.userResponses[this.questionIndex] = [];
            this.next();
        },
        // Go to previous question
        prev: function() {
            this.questionIndex--;
        },

        // Helpers
        // declOfNum(count, ['найдена', 'найдено', 'найдены']);
        declOfNum: function (number, titles) {
            cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        },
        number_format: function (number, decimals, dec_point, thousands_sep) {
            // http://kevin.vanzonneveld.net
            // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +     bugfix by: Michael White (http://getsprink.com)
            // +     bugfix by: Benjamin Lupton
            // +     bugfix by: Allan Jensen (http://www.winternet.no)
            // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // +     bugfix by: Howard Yeend
            // +    revised by: Luke Smith (http://lucassmith.name)
            // +     bugfix by: Diogo Resende
            // +     bugfix by: Rival
            // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
            // +   improved by: davook
            // +   improved by: Brett Zamir (http://brett-zamir.me)
            // +      input by: Jay Klehr
            // +   improved by: Brett Zamir (http://brett-zamir.me)
            // +      input by: Amir Habibi (http://www.residence-mixte.com/)
            // +     bugfix by: Brett Zamir (http://brett-zamir.me)
            // +   improved by: Theriault
            // +   improved by: Drew Noakes
            // *     example 1: number_format(1234.56);
            // *     returns 1: '1,235'
            // *     example 2: number_format(1234.56, 2, ',', ' ');
            // *     returns 2: '1 234,56'
            // *     example 3: number_format(1234.5678, 2, '.', '');
            // *     returns 3: '1234.57'
            // *     example 4: number_format(67, 2, ',', '.');
            // *     returns 4: '67,00'
            // *     example 5: number_format(1000);
            // *     returns 5: '1,000'
            // *     example 6: number_format(67.311, 2);
            // *     returns 6: '67.31'
            // *     example 7: number_format(1000.55, 1);
            // *     returns 7: '1,000.6'
            // *     example 8: number_format(67000, 5, ',', '.');
            // *     returns 8: '67.000,00000'
            // *     example 9: number_format(0.9, 0);
            // *     returns 9: '1'
            // *    example 10: number_format('1.20', 2);
            // *    returns 10: '1.20'
            // *    example 11: number_format('1.20', 4);
            // *    returns 11: '1.2000'
            // *    example 12: number_format('1.2000', 3);
            // *    returns 12: '1.200'
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                toFixedFix = function (n, prec) {
                    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                    var k = Math.pow(10, prec);
                    return Math.round(n * k) / k;
                },
                s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        },
        price: function (string) {
            /*var arr = string.split(' '),
                price = arr[1];*/
            return this.number_format(string, 0, '', ' ') + ' руб.';
        },
        showPage: function (page) {
            this.scrollTop();
            this.currentPage = page;
        },
        objToArray: function (object) {
            var array = [];
            for (var property in object) {
                if (object.hasOwnProperty(property)) {
                    array.push(object[property]);
                }
            }
            return array;
        },
        shuffleArray: function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        },
        convertTimestamp: function(timestamp) {
            var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
                yyyy = d.getFullYear(),
                mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
                dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
                hh = d.getHours(),
                h = hh,
                min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
                ampm = 'AM',
                time;

            if (hh > 12) {
                h = hh - 12;
                ampm = 'PM';
            } else if (hh === 12) {
                h = 12;
                ampm = 'PM';
            } else if (hh == 0) {
                h = 12;
            }

            // ie: 2013-02-18, 8:35 AM
            //time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
            time = dd+ '/' + mm + '/' + yyyy;

            return time;
        },
        scrollTop: function () {
            window.scrollTo(0, 0);
        },
        goToLastQuestion: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            var lastQuestionIndex = this.userResponses.filter(function(val) { return val }).length;
            this.go(lastQuestionIndex);
        },
        goToNextQuestion: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            this.next();
        },
        goToResults: function () {
            this.scrollTop();
            this.findResults(); // Update results on every step
            this.finish();
        },
        changeCurrentVariation: function (skuBaseID) {
            var availableResults = this.availableResults;
            for (var i = 0; i < availableResults.length; i++) {
                var availableResult = availableResults[i];
                if (parseInt(availableResult['sku_base_id']) === parseInt(skuBaseID)) {
                    this.currentVariation = availableResult;
                    break;
                }
            }
        },
        showError: function (el, msg) {
            var inputEl = document.getElementById(el);
            var errorEl = document.createElement('div');
            errorEl.innerHTML = msg;
            //errorEl.style.top = inputEl.offsetTop - 17 + 'px';
            errorEl.style.top = (parseInt(inputEl.offsetTop) + parseInt(inputEl.offsetHeight)) + 'px';
            errorEl.style.left = inputEl.offsetLeft + 'px';
            errorEl.className += " h-error";

            //console.log(inputEl.offsetLeft);
            //inputEl.parentNode.insertBefore(errorEl, inputEl.firstChild);
            inputEl.parentNode.appendChild(errorEl);

            setTimeout(function () {
                errorEl.parentNode.removeChild(errorEl);
            }, 3000);

            // append theKid to the end of theParent
            //theParent.appendChild(theKid);

            // prepend theKid to the beginning of theParent
        },
        prepareShareUrl: function () {

            // http://example.com/?share=1&product=PRODBASEID&image=FILENAME.png&q1=1&q2=1&q3=2&q4=1&q5=1+7

            var queryString = window.location.href + '?share=1';

            queryString += '&product=' + this.results.bigProduct['id'];

            var arr = this.results.bigProduct.image.split('/'),
                image = arr[arr.length - 1];

            queryString += '&image=' + image;

            for (var i = 1; i <= this.userResponses.length; i++) {
                queryString += '&q' + i + '=' + this.userResponses[i-1].join('+');
            }

            //var queryString = 'http://www.clinique.ru/tonalnie-sredstva#4';

            return queryString;
        },
        share: function (type) {
            var resUrl = this.prepareShareUrl();
            //console.log(resUrl);

            switch (type) {
                case 'vk':
                    url = 'http://vkontakte.ru/share.php?';
                    url += 'url=' + encodeURIComponent(resUrl);
                    url += '&noparse=false';
                    break;
                case 'fb':
                    url = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(resUrl);
                    break;
            }


            if (url != '')
                window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        },
        debug: function() {
            //console.log(this.questionIndex);
            //this.findResults();
            // console.log(this.availableResults);
            //console.log(this.userResponses);
            //return 0;
            //return this.userResponses.filter(function(val) { return val }).length;
        },

        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////

        findResults: function () {

            if (!this.availableResults.length)
                return;

            var self = this;

            self.availableVariations = self.availableResults.filter(function (result) {
                var userResponses = self.userResponses.filter(function(val) { return val });
                for (var i = 0; i < userResponses.length; i++) { // пропускаем последний вопрос
                    var answers = userResponses[i],
                        questionIndex = i + 1;

                    if (answers === null || answers.length === 0 || questionIndex === 6)
                        continue;

                    for (var j=0; j<answers.length; j++) {

                        var answer = parseInt(answers[j]);

                        if (result['question_' + questionIndex] instanceof Array) {
                            var tempArray = result['question_' + questionIndex].filter(function(val) { return val });
                            if  (tempArray.indexOf(parseInt(answer)) === -1)
                                return false;
                        }
                        else {
                            if (parseInt(result['question_' + questionIndex]) !== parseInt(answer))
                                return false;
                        }

                    }
                }

                //console.log('answers', self.availableVariations);


                // Фильтруем чекбоксы
                if (typeof userResponses[5] !== "undefined" && userResponses[5] !== null && userResponses[5].length > 0) {
                    var answersForLastQuestion = userResponses[5],
                        availableResultsForLastQuestion = result['question_6'].filter(function(val) {
                            return answersForLastQuestion.indexOf(val + "") !== -1;
                        });
                    return availableResultsForLastQuestion.length > 0
                }

                return true;
            });

        },
        loadMoreProducts: function () {
            var self = this,
                moreProducts = [],
                products = self.shuffleArray(self.results['empty']['ids']);

            self.results['empty']['filtered'] = [];

            if (products.length > 3)
                products.splice(3, products.length - 1);

            for (var i = 0; i < products.length; i++) {
                var prodBaseId = products[i];
                moreProducts.push(self.productsFromDB[prodBaseId]);
            }
            return moreProducts;
        },
        clearPrevResults: function () {
            this.results = {
                'bigProduct': {
                    'id': 0,
                    'title': '',
                    'image': '',
                    'description': '',
                    'weight': 0,
                    'price': 0,
                    'lh1': '',
                    'lh2': '',
                    'review1': '',
                    'review2': '',
                    'variations': [],
                    'variationsAll': [],
                    'variationsMain': [],
                }, // with variations
                'smallProducts': [], // without variations
                'lifehack': 0,
                'empty': {
                    'ids': [
                        9285,
                        6201,
                        42938,
                        5276,
                        39658,
                        45511
                    ],
                    'filtered': []
                },
            };
            this.results['empty']['filtered'] = this.loadMoreProducts();
        },
        prepareResults: function () {
            var self = this,
                prodbaseIDs = [];
            //console.log('prepareResults');

            // prepare products for empty result
            this.results['empty']['filtered'] = self.loadMoreProducts();

            // find variations with sku
            // var filteredVariations = self.products.filter(function (product) {
            //     var variations = self.availableVariations;
            //     for (var i = 0; i < variations.length; i++) {
            //         var variation = variations[i];
            //         if (product['g:mpn'][0] === 'SKU' + variation['sku_base_id']) {
            //             return true;
            //         }
            //     }
            //     return false;
            // });

            // return nothing if we found nothing
            // if (!filteredVariations.length) {
            //     self.clearPrevResults();
            //     return;
            // }

            if (!self.availableVariations.length) {
                self.clearPrevResults();
                return;
            }

            // get prodbaseIDs from available variations
            prodbaseIDs = self.availableVariations.map(function (product) {
                //return parseInt(product['g:prodbaseID'][0]);
                return parseInt(product['prod_base_id']);
            });
            // remove duplicates
            prodbaseIDs = prodbaseIDs.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
            // shuffle IDs
            prodbaseIDs = self.shuffleArray(prodbaseIDs);

            // products with lifehack should be first
            var productWithLifeHackIDs = [];
            // find variations with lifehack
            var variationsWithLifeHack = self.availableVariations.filter(function (variation) {
                return parseInt(variation['lh']) > 0;
            });
            if (variationsWithLifeHack.length > 0) {
                // get prodbaseIDs
                productWithLifeHackIDs = variationsWithLifeHack.map(function (product) {
                    return product['prod_base_id'];
                });
            }

            var bigProductID = 0,
                lifeHackIndex = 0,
                loadedProdbaseID = 0;

            if (parseInt(self.userResult) > 0) {
                var userResult = this.userResults.filter(function(val) { return parseInt(val['id']) === parseInt(self.userResult) });
                loadedProdbaseID = userResult[0]['prod_base_id'];
            }

            if (loadedProdbaseID > 0 && prodbaseIDs.indexOf(loadedProdbaseID) > -1 && self.results['bigProduct']['id'] === 0) {
                bigProductID = loadedProdbaseID;
            }
            else {
                // if we have products with lifehack
                if (productWithLifeHackIDs.length > 0) {
                    // get one of them
                    bigProductID = productWithLifeHackIDs[Math.floor(Math.random()*productWithLifeHackIDs.length)];
                }
                else {
                    // get one ID from prodbaseIDs
                    bigProductID = prodbaseIDs[Math.floor(Math.random()*prodbaseIDs.length)];
                }
            }


            if (parseInt(self.userResult) === -1) {
                bigProductID = parseInt(self.getParameterByName('product'));
            }

            // check if everything is ok
            if (bigProductID === 0) {
                self.clearPrevResults();
                return;
            }

            // set lifehack number
            /*if (variationsWithLifeHack.length > 0) {
                for (var i = 0; i < variationsWithLifeHack.length; i++) {
                    var variationWithLifeHack = variationsWithLifeHack[i];
                    if (parseInt(variationWithLifeHack['prod_base_id']) === parseInt(bigProductID)) {
                        lifeHackIndex = variationWithLifeHack['lh'];
                        break;
                    }
                }
            }*/

            // remove bigProductID from prodbaseIDs
            var bigProductIDIndex = prodbaseIDs.indexOf(bigProductID);
            if (bigProductIDIndex > -1)
                prodbaseIDs.splice(bigProductIDIndex, 1);

            // remove IDs if more then 3
            if (prodbaseIDs.length > 3)
                prodbaseIDs = prodbaseIDs.splice(3, prodbaseIDs.length - 3);

            self.results['bigProduct'] = self.productsFromDB[bigProductID];

            // we need variationsTree to attach properties to bigProduct variations
            var variationsTree = {};
            for (var i = 0; i < self.availableVariations.length; i++) {
                var variation = self.availableVariations[i],
                    prodBaseId = variation['prod_base_id'],
                    SKUBaseId = 'SKU' + variation['sku_base_id'];
                if (typeof variationsTree[prodBaseId] === "undefined")
                    variationsTree[prodBaseId] = {};
                if (typeof variationsTree[prodBaseId][SKUBaseId] === "undefined")
                    variationsTree[prodBaseId][SKUBaseId] = {};
                variationsTree[prodBaseId][SKUBaseId] = variation;
            }

            // get bigProduct variations from feed
            var variationSKUs = [];
            var variations = self.products.filter(function (variation) {
                return parseInt(variation['g:prodbaseID'][0]) === parseInt(self.results['bigProduct']['id']);
            });
            if (variations.length > 0) {
                variationSKUs = variations.map(function (product) {
                    return parseInt(product['g:mpn'][0].replace('SKU', ''));
                });
            }

            // get bigProduct variations from results (main variations)
            var variationsMain = self.availableVariations.filter(function (variation) {
                return parseInt(variation['prod_base_id']) === parseInt(self.results['bigProduct']['id']);
            });
            // set variationsMain
            var variationsMainSKUs = [],
                bigProductVariationsMain = [],
                currentVariation = {};
            if (variationsMain.length > 0 && variationSKUs.length > 0) {
                for (var i = 0; i < variationsMain.length; i++) {
                    var variation = variationsMain[i],
                        variationId = variation['sku_base_id'];
                    // check if user can buy variation
                    variation['canBuy'] = variationSKUs.indexOf(variationId) > -1;

                    // show some variations
                    bigProductVariationsMain.push(variation);
                    // and remember them
                    variationsMainSKUs.push(variationId);

                    if (i === 0)
                        currentVariation = variation;
                }
            }
            // set default variation
            self.currentVariation = currentVariation;

            self.results['bigProduct']['variationsMain'] = bigProductVariationsMain;

            // get bigProduct variations from db (all variations)
            var variationsAll = self.availableResults.filter(function (variation) {
                return parseInt(variation['prod_base_id']) === parseInt(self.results['bigProduct']['id']);
            });
            var bigProductVariationsAll = [],
                bigProductVariationsMore = [];
            if (variationsAll.length > 0 && variationSKUs.length > 0) {
                for (var i = 0; i < variationsAll.length; i++) {
                    var variation = variationsAll[i],
                        variationId = variation['sku_base_id'];

                    // check if user can buy variation
                    variation['canBuy'] = variationSKUs.indexOf(variationId) > -1;

                    //
                    bigProductVariationsAll.push(variation);

                    // hide other variations
                    if (variationsMainSKUs.indexOf(variationId) === -1) {
                        bigProductVariationsMore.push(variation);
                    }

                }
            }

            self.results['bigProduct']['variationsAll'] = bigProductVariationsAll;
            self.results['bigProduct']['variationsMore'] = bigProductVariationsMore;

            // set small products
            var smallProducts = [];
            for (var i = 0; i < prodbaseIDs.length; i++) {
                var prodbaseID = prodbaseIDs[i];
                if (typeof self.productsFromDB[prodbaseID] !== "undefined")
                    smallProducts.push(self.productsFromDB[prodbaseID]);
            }
            self.results['smallProducts'] = smallProducts;

            //
            //
            //~~~
            //console.log('results', self.results);

        },
        finish: function () {
            this.prepareResults();
            this.showPage('results');
            this.questionIndex = this.quiz.questions.filter(function(val) { return val }).length;
            //console.log('end!');
        },
        // check radios and checkboxes
        setValues: function (answers) {
            var inputs = document.getElementsByClassName('js-answer');
            for(var i = 0; i<inputs.length; i++){
                var fieldName = inputs[i].name;
                if (typeof answers[fieldName] !== "undefined") {
                    var fieldValue = inputs[i].value;
                    if (answers[fieldName].indexOf(fieldValue) > -1) {
                        inputs[i].checked = true;
                    }
                }
            }
        },
        // Show user's result
        openResults: function () {
            var self = this;

            if (!self.userEmail) {
                self.showError('email-bottom', 'Пожалуйста, введите свой email.');
                self.showError('email-top', 'Пожалуйста, введите свой email.');
                return;
            }

            $.ajax({
                url: '/load',
                data: {email: self.userEmail},
                success: function (data) {
                    if (data && data.results) {
                        var results = data.results;

                        if (results.length === 0) {
                            self.showError('email-bottom', 'Попробуйте другой email или пройдите тест заново.');
                            self.showError('email-top', 'Попробуйте другой email или пройдите тест заново.');
                            self.userEmail = '';
                            return;
                        }

                        if (results.length > 3)
                            results.splice(0,  data.results.length - 3);

                        for (var i = 0; i < results.length; i++) {
                            results[i]['created_at'] = self.convertTimestamp(results[i]['created_at']);
                            results[i]['total'] = 'Найдено ' + results[i]['total'] + ' ' + self.declOfNum(parseInt(results[i]['total']), ['средство', 'средства', 'средств']);
                        }
                        self.userResults = results;
                        self.userEmail = '';
                        self.showPage('load');
                        //self.showResults = true;
                    }

                }
            });
        },
        //
        // loadResults: function () {
        //     this.questionIndex++;
        // },

        load: function (index) {
            var arr = this.userResults.filter(function(val) { return val });
            for (var i=0; i<arr.length; i ++) {
                if (parseInt(arr[i]['id']) === parseInt(index)) {
                    this.userResult = arr[i].id;
                    this.userResponses = JSON.parse(arr[i].answers);
                    this.goToResults();
                    break;
                }
            }
            this.setValues(this.userResponses);
        },



        /*openSaveForm: function () {
            this.showPage('save');
            this.showSaveForm = true;
        },*/
        openSuccess: function () {
            this.userResult = 0; // set user result to 0 after saving
            this.showPage('success');
            //this.showSuccess = true;
        },
        showMenu: function () {
            this.showEditMenu = !this.showEditMenu;
        },


        getParameterByName: function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },


        saveNotificationEmail: function () {
            var self = this;

            if (!self.notificationEmail) {
                self.showError('notification-email', 'Пожалуйста, введите свой email.');
                return;
            }

            if (self.notificationEmail && self.currentVariation && typeof self.currentVariation['sku_base_id'] !== "undefined") {
                var params = {
                    'email': self.notificationEmail,
                    'sku': self.currentVariation['sku_base_id'],
                    'prodbaseid': self.currentVariation['prod_base_id']
                };

                $.ajax({
                    url: '/notify',
                    data: params,
                    success: function (data) {
                        self.showNotificationModal = false;
                        self.showNotificationSuccessModal = true;
                    }
                });
            }
            else {
                alert('Ошибка! Попробуйте позже...');
            }

        },


        initOwl: function () {
            setTimeout(function () {
                if ($('.js-owl').length > 0) {
                    var owl = $('.js-owl');
                    if ($(window).width() < 641) {
                        if ($('.js-owl .owl-item').length > 0) {
                            $('.js-owl').html('');
                            owl.trigger('refresh.owl.carousel');
                            owl.trigger('destroy.owl.carousel');
                        }
                    }
                    else {

                        if ($('.js-owl .owl-item').length > 0) {
                            $('.js-owl').html('');
                            owl.trigger('refresh.owl.carousel');
                            owl.trigger('destroy.owl.carousel');
                        }

                        owl.owlCarousel({
                            margin: 0,
                            items: 1,
                            loop: false,
                            nav: true,
                            dots: false,
                            autoHeight: true,
                            //nestedItemSelector: '.owl-item',
                            //stageElement: 'div.js-owl',
                            navText: [
                                '<i class="fa fa-angle-left"></i>',
                                '<i class="fa fa-angle-right"></i>'
                            ],
                            autoWidth: false
                        });

                        $('.cl-card__owl-item').each(function (index) {
                            owl.trigger('add.owl.carousel', ['<div class="owl-item">' + $(this).html() + '</div>']).trigger('refresh.owl.carousel');
                        });
                    }
                }
            }, 400);
        }

    },
    watch: {
        isEnd: function (val, oldVal) {
            if (val === true) {
                this.initOwl();
            }
        },
        currentPage: function (val, oldVal) {
            if (oldVal !== val) {
                this.initOwl();
            }
        }
    },
});