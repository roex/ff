'use strict';

/**
 * Dependencies
 */

var	gulp = require('gulp'),
		fileinclude = require('gulp-file-include'),
		watch = require('gulp-watch'),
		prefixer = require('gulp-autoprefixer'),
		uglify = require('gulp-uglify'),
		sourcemaps = require('gulp-sourcemaps'),
		less = require('gulp-less'),
		concat = require('gulp-concat'),
		rigger = require('gulp-rigger'),
		cssmin = require('gulp-minify-css'),
		rimraf = require('rimraf'),
		browserSync = require("browser-sync"),
		globPlugin = require('less-plugin-glob'),
		reload = browserSync.reload,
        fs = require('fs'),
		buildPath = 'build/',
        srcPath = 'assets/';

/**
 * Variables & config definition
 */

var path = {
	build: {
        html: buildPath,
        js: buildPath + '/js/',
        css: buildPath + '/css/',
        img: buildPath + '/img/',
        images: buildPath + '/images/',
        media: buildPath + '/media/',
        data: buildPath + '/data/',
        fonts: buildPath + '/fonts/'
	},
	src: {
		html: srcPath + 'template/pages/*.html',
        js: srcPath + 'js/main.js',
        appjs: srcPath + 'js/app.js',
        vendorjs: srcPath + 'js/vendor.js',
        style: srcPath + 'style/main.less',
        img: srcPath + 'img/**/*.*',
        images: srcPath + 'images/**/*.*',
        media: srcPath + 'media/**/*.*',
        data: srcPath + 'data/**/*.*',
        fonts: srcPath + 'fonts/**/*.*'

	},
	watch: {
        html: srcPath + 'template/**/*.html',
        js: srcPath + 'js/**/*.js',
        style: srcPath + 'style/**/*.less',
        templatestyle: srcPath + 'template/**/*.less',
        img: srcPath + 'img/**/*.*',
        images: srcPath + 'images/**/*.*',
        media: srcPath + 'media/**/*.*',
        data: srcPath + 'data/**/*.*',
        fonts: srcPath + 'fonts/**/*.*'
	},
	clean: './' + buildPath
};

var config = {
	server: {
		baseDir: "./" + buildPath
	},
	watchOptions: {
		usePolling: true
	},
	open: false,
	tunnel: false,
	port: 9000
};

/**
 * Build section
 */

gulp.task('webserver', function () {
	browserSync(config);
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
	gulp.src(path.src.js)
		.pipe(rigger())
		.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('appjs:build', function () {
	gulp.src(path.src.appjs)
		.pipe(rigger())
		//.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('vendorjs:build', function () {
	gulp.src(path.src.vendorjs)
		.pipe(rigger())
		.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
		.pipe(less({
			plugins: [ globPlugin ]
		}))
		.pipe(concat('main.css'))
		.pipe(prefixer({
			browsers: ['ie >= 7', 'last 25 versions', '> 0.1%'],
			cascade: false
		}))
		.pipe(cssmin())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});
gulp.task('images:build', function () {
    gulp.src(path.src.images)
        .pipe(gulp.dest(path.build.images))
        .pipe(reload({stream: true}));
});

gulp.task('media:build', function() {
    gulp.src(path.src.media)
        .pipe(gulp.dest(path.build.media))
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'appjs:build',
    'vendorjs:build',
    'style:build',
    'fonts:build',
    'image:build',
    'images:build',
    'media:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.templatestyle], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start(['js:build', 'appjs:build', 'vendorjs:build']);
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.images], function(event, cb) {
        gulp.start('images:build');
    });
    watch([path.watch.media], function(event, cb) {
        gulp.start('media:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);

//
// !!! Create new block or component in template folder !!!
//
// Example: gulp create --name menu --type block/component
//
var args   = require('yargs').argv,
    projectPrefix = 'ku-',
    folder = '',
    type = args.type,
    name = args.name;
gulp.task('create', function () {
    switch (type) {
        case 'block':
            folder = 'blocks';
            break;
        case 'component':
            folder = 'components';
            break;
        default:
            folder = 'blocks';
    }
    var dir = './assets/template/' + folder + '/' + name;
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);

        var lessContent = '/**\n'+
                            '* Main styles\n'+
                            '**/\n\n'+
                            '.'+projectPrefix+name+' {}\n'+
                            '\n'+
                            '/**\n'+
                            '* Responsive styles\n'+
                            '**/\n'+
                            '\n'+
                            '// @media (min-width: @screen-md-min) {\n'+
                            '//\n'+
                            '//}\n'+
                            '\n'+
                            '//@media (max-width: @screen-sm-max) {\n'+
                            '//\n'+
                            '//}\n'+
                            '\n'+
                            '//@media (max-width: @screen-xs-max) {\n'+
                            '//\n'+
                            '//}\n',
        htmlContent = '<!--.'+projectPrefix+name+'-->\n<div class="' + projectPrefix + name + '">\n\n</div>\n<!--/.'+projectPrefix+name+'-->';

        fs.writeFileSync(dir + '/' + name + '.less', lessContent);
        fs.writeFileSync(dir + '/index.html', htmlContent);
    } else {
        console.log("Error: Directory already exist!");
    }
});
