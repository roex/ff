////
var auth = require('./auth');
////

var express = require('express');
var assets = require('express-asset-versions')
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var routes = require('./routes/index');
//var sendcode = require('./routes/sendcode');
//var rules = require('./routes/rules');
//var share = require('./routes/share');
var products = require('./routes/products');
var save = require('./routes/save');
var load = require('./routes/results');
var variations = require('./routes/variations');
var feed = require('./routes/feed');
var notify = require('./routes/notify');
var download = require('./routes/download');



var app = express();

// protect direct access
app.get('/storage/csv/*', function(req, res, next) {
    res.end('You are not allowed!');
});

//app.set('strict routing', true);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'hbs');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


//app.use(favicon(path.join(__dirname, 'public/images/', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var assetPath = path.join(__dirname, 'public');
app.use( express.static(assetPath));
app.use(assets('/', assetPath));

//app.use('/share', share);
app.use('/feed', feed);
app.use('/variations', variations);
app.use('/products', products);

app.use('/save', save);
app.use('/load', load);
//app.use('/preset', preset);

app.use('/notify', notify);
app.use('/download', download);


//app.use('/sendcode', sendcode);
//app.use('/rules', rules);



app.use('/', routes);

app.use(auth);

// protect csv files
app.get('/get/*', function(req, res) {
    //res.sendFile(__dirname + '/public/storage/csv/1507116573993.csv');
    //console.log(req.params);
    res.sendFile(__dirname + '/public/storage/csv/' + req.params[0] + '.csv');
    //res.send({'msg': 'OK!'});
    //res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.status(err.status || 500);
//   res.render('error', {
//     message: err.message,
//     error: {}
//   });
// });


module.exports = app;
